package com.bitskey.grossbit.chat.chat2;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bitskey.grossbit.chat.gen.ChatUsersListActivity;
import com.bitskey.grossbit.chat.gen.ChatXMPPService;
import com.bitskey.grossbit.chat.gen.FileTransferWrapper;
import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.gen.Utility;
import com.bitskey.grossbit.chat.db.ChatRecord;
import com.bitskey.grossbit.chat.db.ChatRecordCache;
import com.bitskey.grossbit.chat.db.DataSource;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.chat.ChatManagerListener;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Vector;
//import org.apache.commons.httpclient.methods.*;
//import org.apache.commons.httpclient.params.HttpMethodParams;

//import org.jivesoftware.smack.chat.ChatMessageListener;
//For Creating user: Manish

//nsw
public class MainActivity extends Activity {
    private static final String TAG = "ChatActivity";
    AbstractXMPPConnection mConnection = null;
    //ChatMessageListener myMessageListener = null;
    ChatManagerListener myChatListener = null;

    Button mButton = null;
    EditText mChatText = null;
    //TextView mMyName = null;
    TextView mRemoteUserName = null;
    TextView mChatHistoryText = null;

    static String myNameStr;
    static String remoteNameStr;
    //static String historyTextStr;

    private final String FILENAME = "GBC: MainActivity#";
    public static String MESSAGETYPETEXT = "Text";
    public static String MESSAGETYPEIMAGE = "Image";
    public static String MESSAGETYPESTREAM = "Stream";
    public static int MESSAGEDIRECTIONRECEIVED = 1;
    public static int MESSAGEDIRECTIONSENT = 0;

    private static final int SELECT_PICTURE = 1;
    private String selectedImagePath;
    private ImageView img;

    private static final int PICTURE_GALLERY = 1;
    private static final int FILE_SELECT_CODE = 2;

    private MessageListener myMessageListener;

    private ChatListLayout simpleAdpt;
    ListView lv;
    public static List<ChatRecordCache> chatCacheList;
    private boolean mBoolLoadDB;


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(FILENAME, "onDestroy");
        //bManager.unregisterReceiver(bReceiver);

        finishActivity(0);
        mBoolLoadDB = false;
    }

    @Override
    protected void onNewIntent(Intent intent){
        onHandleIntent(intent);
    }

    /*
    @Override
    protected void onPause()
    {
        super.onPause();
        // Store values between instances here
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        TextView myNameTextView = (TextView)findViewById(R.id.chat_myName);
        myNameStr = myNameTextView.getText().toString();

        TextView remoteNametextview = (TextView)findViewById(R.id.chat_remoteName);
        remoteNameStr = remoteNametextview.getText().toString();

        TextView historyTextView = (TextView)findViewById(R.id.chat_history_text);
        historyTextStr = historyTextView.getText().toString();

        editor.putString(myNameStr,"myNameStr"); // value to store
        editor.putString(remoteNameStr, "remoteNameStr"); // value to store
        editor.putString(historyTextStr, "historyTextStr"); // value to store

        // Commit to storage
        editor.commit();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        // Store values between instances here
        SharedPreferences preferences = getPreferences(MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();  // Put the values from the UI

        preferences.getString("myNameStr", myNameStr); // value to store
        preferences.getString(remoteNameStr, "remoteNameStr"); // value to store
        preferences.getString("historyTextStr", historyTextStr); // value to store

        if ( myNameStr!=null && !myNameStr.isEmpty()) {
            TextView myNameTextView = (TextView) findViewById(R.id.chat_myName);
            myNameTextView.setText(myNameStr);
        }

        if (remoteNameStr!=null && !remoteNameStr.isEmpty()) {
            TextView remoteNametextview = (TextView) findViewById(R.id.chat_remoteName);
            remoteNametextview.setText(remoteNameStr);
        }

        if (historyTextStr!=null && !historyTextStr.isEmpty()) {
            TextView historyTextView = (TextView) findViewById(R.id.chat_history_text);
            historyTextView.setText(historyTextStr);
        }
    }
    */

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        Log.d(FILENAME, "onSaveInstanceState");

        TextView remoteNametextview = (TextView)findViewById(R.id.chat_remoteName);
        remoteNameStr = remoteNametextview.getText().toString();
        savedInstanceState.putString("myNameStr", myNameStr);
        savedInstanceState.putString("remoteNameStr", remoteNameStr);
        Log.d(FILENAME, "name# " + myNameStr + "remotename# " + remoteNameStr);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.
        myNameStr = savedInstanceState.getString("myNameStr");
        remoteNameStr = savedInstanceState.getString("remoteNameStr");
        Log.d(FILENAME, "onRestoreInstanceState /n name# " + myNameStr + "remotename# " + remoteNameStr);

        TextView remoteNametextview = (TextView)findViewById(R.id.chat_remoteName);
        remoteNametextview.setText(remoteNameStr);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(FILENAME, "onCreate");

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatXMPPService.ACTION_RECEIVEDMESSAGE);
        mBoolLoadDB = true;
        if (savedInstanceState!=null) {
            myNameStr = savedInstanceState.getString("myNameStr");
            remoteNameStr = savedInstanceState.getString("remoteNameStr");
            Log.d(FILENAME, "onCreate SavedInstanceState not null");
            Log.d(FILENAME, "name# " + myNameStr + "remotename# " + remoteNameStr);
            TextView remoteNametextview = (TextView) findViewById(R.id.chat_remoteName);
            remoteNametextview.setText(remoteNameStr);
        }
        mButton = (Button) findViewById(R.id.send_txt_button);
        mChatText = (EditText) findViewById(R.id.chat_text_box);

        //img = (ImageView) findViewById(R.id.ivPic);
        ((Button) findViewById(R.id.bBrowse))
                .setOnClickListener(    new View.OnClickListener() {
                    public void onClick(View arg0) {
                        /*
                        Intent intent = new Intent();
                        intent.setType("* /*"); //remove space after *
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        try {
                            startActivityForResult(
                                    Intent.createChooser(intent, "Select a File to Upload"),
                                    PICTURE_GALLERY);
                        } catch (android.content.ActivityNotFoundException ex) {
                            Log.d(FILENAME, "Error, excception in file chooser");
                            ex.printStackTrace
                            // Potentially direct the user to the Market with a Dialog
                            //Toast.makeText(this, "Please install a File Manager.", Toast.LENGTH_SHORT).show();
                        }
                        */
                        //Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        //photoPickerIntent.setType("*/*");
                        //photoPickerIntent.addCategory(Intent.CATEGORY_OPENABLE);
                        Intent intent;
                        if (Build.VERSION.SDK_INT < 19) {
                            intent = new Intent();
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            intent.setType("*/*");
                            startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
                        } else {
                            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                            intent.addCategory(Intent.CATEGORY_OPENABLE);
                            intent.setType("*/*");
                            startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
                        }

                        //Following works fine:
                        //Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        //startActivityForResult(photoPickerIntent, PICTURE_GALLERY);
                    }
                });
        ;

        //Sending logic
        mButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (mChatText.getText().toString() != null) {
                    mChatText.setMovementMethod(new ScrollingMovementMethod());
                    Log.d(FILENAME, "setOnClickListener: Sending to" + mRemoteUserName.getText().toString());
                    String textHistory = ChatXMPPService.mUserName + ": ";
                    textHistory += mChatText.getText().toString() + "\n";
                    storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPETEXT, textHistory, null);
                    ChatXMPPService.startActionSendMessage(getApplicationContext(), mChatText.getText().toString(), mRemoteUserName.getText().toString(), MESSAGETYPETEXT);                             // Perform action on click

                    if (mBoolLoadDB) {
                        //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                        loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                    }
                    lv = (ListView) findViewById(R.id.chatlistView);
                    //For now groups are false
                    simpleAdpt = new ChatListLayout(getApplicationContext(), R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                    lv.setAdapter(simpleAdpt);
                    lv.setScrollY(0);
                }
            }
        });
        onHandleIntent(getIntent());
    }

    // Following two functions are for
    // For uploading image:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        String filePath = null;
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = intent.getData();
                    Log.d(FILENAME, "File Uri: " + uri.toString());
                    // Get the path
                    try {
                        filePath = Utility.getPath(this, uri);
                        Log.d(FILENAME, "File Path: " + filePath);
                    }
                    catch (URISyntaxException e)
                    {
                        Log.d(FILENAME, "Exception in getting path for uri " + uri);
                        e.printStackTrace();
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload

                    //filePath = cursor.getString(columnIndex);
                    selectedImagePath = filePath;
                    //cursor.close();
                    //Bitmap the_image = decodeFile(new File(filePath));
                    String textHistory = ChatXMPPService.mUserName + ": ";
                    textHistory += selectedImagePath.toString() + "\n";

                    storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                    //storeMessage(ChatXMPPService.mUserName, ChatXMPPService.mUserName, MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                    if ( mBoolLoadDB) {
                        //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                        loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                    }
                    lv = (ListView) findViewById(R.id.chatlistView);
                    //For now groups are false
                    simpleAdpt = new ChatListLayout(getApplicationContext(), R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                    lv.setAdapter(simpleAdpt);
                    lv.setScrollY(0);
                    ChatXMPPService.startActionSendMessage(getApplicationContext(), selectedImagePath, mRemoteUserName.getText().toString(), MESSAGETYPEIMAGE);// Perform action on click
                }
                break;

            case PICTURE_GALLERY:
                if (resultCode == RESULT_OK && intent != null) {
                    Uri selectedImage = intent.getData();
                    final String[] filePathColumn = { MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME };
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // some devices (OS versions return an URI of com.android instead of com.google.android
                    if (selectedImage.toString().startsWith("content://com.android.gallery3d.provider"))  {
                        // use the com.google provider, not the com.android provider.
                        selectedImage = Uri.parse(selectedImage.toString().replace("com.android.gallery3d","com.google.android.gallery3d"));
                    }
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
                        // if it is a picasa image on newer devices with OS 3.0 and up
                        if (selectedImage.toString().startsWith("content://com.google.android.gallery3d")||
                                selectedImage.toString().startsWith("content://com.android.providers")
                                ){
                            columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
                            if (columnIndex != -1) {
                                //progress_bar.setVisibility(View.VISIBLE);
                                final Uri uriurl = selectedImage;
                                // Do this in a background thread, since we are fetching a large image from the web
                                File f = getBitmap("file_name", uriurl);
                                selectedImagePath  = f.getPath();
                                new Thread(new Runnable() {
                                    public void run() {
                                        FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString() + "@" + ChatXMPPService.DOMAIN);
                                    }
                               }).start();
                                String textHistory =ChatXMPPService.mUserName + ": ";
                                textHistory += selectedImagePath.toString() + "\n";

                                storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, selectedImagePath);
                                //storeMessage(ChatXMPPService.mUserName, ChatXMPPService.mUserName, MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                                if ( mBoolLoadDB) {
                                    //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                                    loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                                }
                                lv = (ListView) findViewById(R.id.chatlistView);
                                //For now groups are false
                                simpleAdpt = new ChatListLayout(getApplicationContext(), R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                                lv.setAdapter(simpleAdpt);
                                lv.setScrollY(0);

                            }
                        } else { // it is a regular local image file
                            filePath = cursor.getString(columnIndex);
                            selectedImagePath = filePath;
                            cursor.close();
                            //Bitmap the_image = decodeFile(new File(filePath));
                            String textHistory = ChatXMPPService.mUserName + ": ";
                            textHistory += selectedImagePath.toString() + "\n";

                            new Thread(new Runnable() {
                                public void run() {
                                    FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString() + "@" + ChatXMPPService.DOMAIN);
                                }
                            }).start();

                            storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                            //storeMessage(ChatXMPPService.mUserName, ChatXMPPService.mUserName, MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                            if ( mBoolLoadDB) {
                                //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                                loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                            }
                            lv = (ListView) findViewById(R.id.chatlistView);
                            //For now groups are false
                            simpleAdpt = new ChatListLayout(getApplicationContext(), R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                            lv.setAdapter(simpleAdpt);
                            lv.setScrollY(0);
                        }
                    }
                    // If it is a picasa image on devices running OS prior to 3.0
                    else if (selectedImage != null && selectedImage.toString().length() > 0) {
                        //progress_bar.setVisibility(View.VISIBLE);
                        final Uri uriurl = selectedImage;
                        File the_image = getBitmap("file_name", uriurl);
                        selectedImagePath  = the_image.getPath();

                        // Do this in a background thread, since we are fetching a large image from the web
                        new Thread(new Runnable() {
                           public void run() {
                                FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString() + "@" + ChatXMPPService.DOMAIN);
                           }
                       }).start();
                        String textHistory = ChatXMPPService.mUserName + ": ";
                        textHistory += selectedImagePath.toString() + "\n";

                        storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, selectedImagePath);
                        //storeMessage(ChatXMPPService.mUserName, ChatXMPPService.mUserName, MESSAGEDIRECTIONSENT, null, MESSAGETYPEIMAGE, textHistory, filePath);
                        if ( mBoolLoadDB) {
                            //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                            loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                        }
                        lv = (ListView) findViewById(R.id.chatlistView);
                        //For now groups are false
                        simpleAdpt = new ChatListLayout(getApplicationContext(), R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                        lv.setAdapter(simpleAdpt);
                        lv.setScrollY(0);
                    }
                }
                /*
                new Thread(new Runnable() {
                    public void run() {
                         FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString() + "@" + ChatXMPPService.DOMAIN);
                    }
                }).start();
                */
                //ChatXMPPService.startActionSendMessage(getApplicationContext(),selectedImagePath, mRemoteUserName.getText().toString(), MESSAGETYPEIMAGE);// Perform action on click
                break;
        }
        //super.onActivityResult(requestCode, resultCode, intent);

    }

    private File getBitmap(String tag, Uri url)
    {
        File cacheDir;
        // if the device has an SD card
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
        } else {
            // it does not have an SD card
            //cacheDir=ActivityPicture.this.getCacheDir();
            cacheDir=this.getCacheDir();
        }
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        //Get FileName
        String fileName = null;
        if (url.getScheme().equals("file")) {
            fileName = url.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(url, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                    Log.d(FILENAME, "name is " + fileName);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }

        File f;
        try {
            Bitmap bitmap=null;
            InputStream is = null;
            if (url.toString().startsWith("content://com.google.android.gallery3d") ||
                    url.toString().startsWith("content://com.android.providers")
                    ) {
                is=getContentResolver().openInputStream(url);
            } else {
                is=new URL(url.toString()).openStream();
            }
            if (fileName!=null) {
                f = new File(cacheDir, fileName);
            }
            else
                f = new File(cacheDir, tag);

            OutputStream os = new FileOutputStream(f);

            //Utils.CopyStream(is, os);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            //f.
            //return null;
            //is.decodeStream

            return f;
        } catch (Exception ex) {
            Log.d(FILENAME, "Exception: " + ex.getMessage());
            // something went wrong
            ex.printStackTrace();
            return null;
        }
    }

    //@Override
    protected void onHandleIntent(Intent intent) {
        Log.d(FILENAME, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (ChatUsersListActivity.ACTION_CHAT_START.equals(action)) {
                myNameStr=ChatXMPPService.mUserName;
                final String param1 = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
                mRemoteUserName.setText(param1);
                remoteNameStr = param1;
                if ( mBoolLoadDB) {
                    //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                    loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                    //FileTransferWrapper.FileReceive();
                }

                // Find a way - where lv is initialized only once
                lv = (ListView) findViewById(R.id.chatlistView);
                //For now groups are false
                simpleAdpt = new ChatListLayout(this, R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                lv.setAdapter(simpleAdpt);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START# myName" + ChatXMPPService.mUserName + "remoteName" + mRemoteUserName);
                lv.setScrollY(0);

            }

            else if (ChatXMPPService.ACTION_RECEIVEDMESSAGE.equals(action)) {
                Log.d(FILENAME, "onHandleIntent: ACTION_RECEIVEDMESSAGE");
                myNameStr=ChatXMPPService.mUserName;
                final String param1 = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
                mRemoteUserName.setText(param1);
                remoteNameStr = param1;

                final String message = intent.getStringExtra(ChatXMPPService.MESSAGERECEIVED);
                mChatText.setMovementMethod(new ScrollingMovementMethod());
                storeMessage(ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, MESSAGETYPETEXT, message, null);

                if ( mBoolLoadDB) {
                    //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                    loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                }

                lv = (ListView) findViewById(R.id.chatlistView);
                //For now groups are false
                simpleAdpt = new ChatListLayout(this, R.layout.activity_main, R.id.username, chatCacheList, ChatXMPPService.mUserName, remoteNameStr, false);
                lv.setAdapter(simpleAdpt);
                lv.setScrollY(0);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(FILENAME, "onCreateOptionsMenu");
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        //createChatConnection();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(FILENAME, "onOptionsItemSelected");
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    protected void loadFromDB(String userName, String remoteUsername, boolean isGroup)
    {
        DataSource db = new DataSource(this);
        db.open();

        List<ChatRecord> reqs = db.getChatRecords(userName, remoteUsername, isGroup);
        if (chatCacheList==null) {
            chatCacheList = new Vector<ChatRecordCache>();
        }
        else
        {
            chatCacheList.clear();
        }
        for(ChatRecord _req : reqs) {
                ChatRecordCache e = new ChatRecordCache(_req.getId(),_req.getUserName(), _req.getGroupName() , _req.getRemoteUserName(),_req.getCreationTimestamp(),
                        _req.getMessageDirection() ,_req.getMessageType(), _req.getMessageText(), _req.getMessageFileName(), _req.getMessageFilePath());
                chatCacheList.add(e);
        }
        db.close();
    }


    protected void storeMessage(String userName, String remoteUserName, int messageDirection, String groupName, String messageType, String messageText, String fileName) {
        ChatRecord req = new ChatRecord();
        req.setUserName(userName);
        req.setRemoteUserName(remoteUserName);
        req.setMessageDirection(messageDirection);
        req.setMessageType(messageType);
        req.setMessageText(messageText);
        req.setMessageFileName(fileName);
        //req.setCreationTimestamp(android.text.format.Time.()); //Store time lately:
        //filepath shall always be same
        if (fileName!=null)
        {
            //req.setMessageFilePath(FILEPATH); //Define Filepath
        }
        DataSource db = new DataSource(this);
        db.open();
        ChatRecordCache e = new ChatRecordCache(req.getId(),req.getUserName(), req.getGroupName() , req.getRemoteUserName(),req.getCreationTimestamp(),
                req.getMessageDirection() ,req.getMessageType(), req.getMessageText(), req.getMessageFileName(), req.getMessageFilePath());
        chatCacheList.add(e);
        db.persistChatRecord(req);
        db.close();
    }
}