package com.bitskey.grossbit.chat.chat2;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bitskey.grossbit.chat.gen.ChatUsersListActivity;
import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.db.ChatRecordCache;

import java.util.List;

/**
 * Created by GUR11219 on 31-07-2015.
 */

public class ChatListLayout extends ArrayAdapter<ChatRecordCache> {
    int groupid;
    Context context;
    String mUserName;
    String mRemoteUserName;
    boolean mIsGroup;
    private final String FILENAME = "GBC ChatListLayout#";
    ChatListLayout(Context context, int vg, int id, List<ChatRecordCache> chatCacheList, String userName, String remoteUserName, boolean isGroup){
        super(context,vg, chatCacheList);
        this.context=context;
        groupid=vg;
        mUserName = userName;
        mRemoteUserName = remoteUserName;
        mIsGroup = isGroup;

    }
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       // View itemView = inflater.inflate(groupid, parent, false);
        View activity_chat_list_view = inflater.inflate(R.layout.activity_main, null);//= inflater
        View userView;
        //View activity_chat_list_view = inflater.inflate(R.layout.chatlistlayout, null);
        Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row ==null) {
            userView = inflateUserListLayout(inflater, activity_chat_list_view, position, false );
        }
        else
        {
            //You get here in case of scroll
            //Currently both the behaviours are defined as same. But it might need to be revisited later.
            userView = inflateUserListLayout(inflater, activity_chat_list_view, position, true );
        }
        return userView;
    }

    public View inflateUserListLayout(LayoutInflater inflater, View activity_chat_list_view, int position, boolean convertview)
    {
        View user_view = null;
        try {
            user_view = inflater.inflate(R.layout.chatlistlayout, (ViewGroup) activity_chat_list_view, false);
            //ImageView imageView = (ImageView) user_view.findViewById(R.id.icon);
            //imageView.setImageDrawable(context.getResources().getDrawable(imageList[position]));
            /*
                TextView chatText = (TextView) user_view.findViewById(R.id.chatText);
                //textuserName.setText(userNamelist[position]);
                //textuserName.setCompoundDrawableWithIntrinsicBounds(yourImg, null, null, null);
                ImageSpan is = new ImageSpan(context, R.drawable.ic_action_heart);
                SpannableString text = new SpannableString("Lorem ipsum dolor sit amet");
                text.setSpan(is, 5, 5 + 10, 0);
                chatText.setText(text);
                chatText.setOnClickListener(new View.OnClickListener() {
                                                    public void onClick(View arg0) {
                                                        startActionChat(ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        Toast.makeText(getContext(), "Clicked ", Toast.LENGTH_SHORT).show();
                                                        return;
                                                    }
                                                }
                );
                ((ViewGroup) activity_chat_list_view).addView(user_view);
            */
                TextView chatText;
                ChatRecordCache req = MainActivity.chatCacheList.get(position);
                if (MainActivity.MESSAGETYPETEXT.equals(req.getMessageType())) {
                    chatText = (TextView) user_view.findViewById(R.id.chatText);
                    /*
                    if (req.getMessageDirection() == MainActivity.MESSAGEDIRECTIONSENT)
                    {
                        //chatText.setTextColor(0x000000);
                        //chatText.setBackgroundColor(0xf1f1f1);
                    }
                    else
                    {
                        //Received Text
                        //chatText.setTextColor(0x0000ff);
                        //chatText.setBackgroundColor(0xffffff);
                    }*/
                    chatText.setText(req.getMessageText());
                    chatText.setOnClickListener(new View.OnClickListener() {
                                                    public void onClick(View arg0) {
                                                        //startActionChat(ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        Toast.makeText(getContext(), "Clicked ", Toast.LENGTH_SHORT).show();
                                                        return;
                                                    }
                                                }
                    );
                }
                /*
                else //if (MainActivity.MESSAGETYPETEXT.equals(req.getMessageType()))
                {
                    //chatText = (TextView) user_view.findViewById(R.id.chatText);
                    ImageView image = (ImageView) user_view.findViewById(R.id.imageView1);
                    AQuery aq = new AQuery(image);
                    String url="http://www.queness.com/resources/images/png/apple_ex.png";
                    //aq.id(image).image(req.getMessageFilePath(), true, true, 0, R.drawable.ic_launcher);
                    aq.id(image).image(url, true, true, 0, R.drawable.ic_launcher);
                    //ImageSpan is = new ImageSpan(context,R.drawable.ic_action_heart);
                    //SpannableString text = new SpannableString("Image Display");
                    //text.setSpan(is, 5, 5 + 10, 0);
                    if (req.getMessageDirection().equals(MainActivity.MESSAGEDIRECTIONSENT))
                    {
                        //chatText.setTextColor(0x000000);
                        //chatText.setBackgroundColor(0xf1f1f1);
                        //chatText.setLeft(1);
                    }
                    else
                    {
                        //Received Text
                        //chatText.setTextColor(0x0000ff);
                        //chatText.setBackgroundColor(0xffffff);
                        //chatText.setLeft(10);
                        //chatText.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                    }
                    //chatText.setText(req.getMessageText());
                }
                */
                ((ViewGroup) activity_chat_list_view).addView(user_view);
                //((ViewGroup) userview).addView(user_view);
                return user_view;
        }catch (Exception e)
        {
            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;

    }
    // TODO: Customize helper method\\\\\\\\\
    void startActionChat(String param1, String param2) {
        Log.d(FILENAME, "startActionChat");
        Intent intent = new Intent(getContext(), MainActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        getContext().startActivity(intent);
    }
}
