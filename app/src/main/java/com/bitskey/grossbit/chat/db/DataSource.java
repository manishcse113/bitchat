package com.bitskey.grossbit.chat.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;

//Manish: Added for CTalk

public class DataSource {

	// Database fields
	public SQLiteDatabase db;
	private DatabaseHelper helper;

	private static String FILENAME = "GBC DataSource";

	public DataSource(Context context) {
		helper = new DatabaseHelper(context);
	}

	public void open() throws SQLException {
		db = helper.getWritableDatabase();
	}

	public void close() {
		helper.close();
	}

	public ChatRecord getChatRecord(long id) {

		//Log.d(FILENAME, "getChatRecord");
		return ChatRecord.loadFromDatabase(db, id);
	}

	public List<ChatRecord> getChatRecords() {
		//Log.d(FILENAME, "getChatRecords");

		return ChatRecord.loadFromDatabase(db);
	}

	public List<ChatRecord> getChatRecords(String userName) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName);
	}

	public List<ChatRecord> getChatRecords(String userName, String receiptStatus) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, receiptStatus);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, remoteUsername,isGroup);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup, String receiptId) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabaseReceiptId(db, userName, remoteUsername, isGroup, receiptId);
	}

	public void persistChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "persistChatRecord");
		chatRecord.commit(db);
	}

	public void deleteChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "deleteChatRecord");
		chatRecord.delete(db);
	}

	public void updateChatRecord (String userName, String remoteUserName, String receiptId, String receiptStatus)
	{
		//Log.d(FILENAME, "updateChatRecord");
		ChatRecord.LoadAndUpdateChatRecord(db, userName, remoteUserName, false, receiptId, receiptStatus);
	}

	public MyRecord getMyRecord(long id) {

		//Log.d(FILENAME, "getMyRecord");
		return MyRecord.loadFromDatabase(db, id);
	}

	public List<MyRecord> getMyRecords() {
		//Log.d(FILENAME, "getMyRecords");
		return MyRecord.loadFromDatabase(db);
	}

	public List<MyRecord> getMyRecords(String userName) {
		//Log.d(FILENAME, "getMyRecords");
		return MyRecord.loadFromDatabase(db, userName);
	}
	public void persistMyRecord(MyRecord myRecord) {

		//Log.d(FILENAME, "persistMyRecord");
		myRecord.commit(db);
	}

	public void deleteMyRecord(MyRecord myRecord) {
		//Log.d(FILENAME, "deleteMyRecord");
		myRecord.delete(db);
	}


	public UserRecord getUserRecord(long id) {
		//Log.d(FILENAME, "getUserRecord");
		return UserRecord.loadFromDatabase(db, id);
	}

	public List<UserRecord> getUserRecords() {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db);
	}

	public List<UserRecord> getUserRecords(String userName) {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db, userName);
	}
	public void persistUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "persistUserRecords");
		userRecord.commit(db);
	}

	public void deleteUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "delteUserRecords");
		userRecord.delete(db);
	}


	/*
	public SIPIdentity getSIPIdentity(long id) {
		return SIPIdentity.loadFromDatabase(db, id);
	}
	
	public List<SIPIdentity> getSIPIdentities() {
		return SIPIdentity.loadFromDatabase(db);
	}
	
	public void persistSIPIdentity(SIPIdentity sipIdentity) {
		sipIdentity.commit(db);
	}
	
	public void deleteSIPIdentity(SIPIdentity sipIdentity) {
		sipIdentity.delete(db);
	}


	public List<PTTChannel> getPTTChannels() {
		return PTTChannel.loadFromDatabase(db);
	}

	public void deletePTTChannel(PTTChannel object) {
		object.delete(db);
	}

	public void persistPTTChannel(PTTChannel object) {
		object.commit(db);
	}

	public PTTChannel getPTTChannel(long id) {
		return PTTChannel.loadFromDatabase(db, id);
	}
	
	public List<ENUMSuffix> getENUMSuffixes() {
		return ENUMSuffix.loadFromDatabase(db);
	}

	public void deleteENUMSuffix(ENUMSuffix object) {
		object.delete(db);
	}

	public ENUMSuffix getENUMSuffix(long id) {
		return ENUMSuffix.loadFromDatabase(db, id);
	}

	public void persistENUMSuffix(ENUMSuffix object) {
		object.commit(db);
	}
	
	//Manish: Added for CTalk, removed sIP5060
	public CTalkRegisterRequest getCTalkRegisterRequest(long id) {
		return CTalkRegisterRequest.loadFromDatabase(db, id);
	}
	
	public List<CTalkRegisterRequest> getCTalkRegisterRequests() {
		return CTalkRegisterRequest.loadFromDatabase(db);
	}
	
	public void persistCTalkRegisterRequest(CTalkRegisterRequest cTalkRegisterRequest) {
		cTalkRegisterRequest.commit(db);
	}
	
	public void deleteCTalkRegisterRequest(CTalkRegisterRequest cTalkRegisterRequest) {
		cTalkRegisterRequest.delete(db);
	}
	
	
	
	public SIP5060ProvisioningRequest getSIP5060ProvisioningRequest(long id) {
		return SIP5060ProvisioningRequest.loadFromDatabase(db, id);
	}
	
	public List<SIP5060ProvisioningRequest> getSIP5060ProvisioningRequests() {
		return SIP5060ProvisioningRequest.loadFromDatabase(db);
	}
	
	public void persistSIP5060ProvisioningRequest(SIP5060ProvisioningRequest sIP5060ProvisioningRequest) {
		sIP5060ProvisioningRequest.commit(db);
	}
	
	public void deleteSIP5060ProvisioningRequest(SIP5060ProvisioningRequest sIP5060ProvisioningRequest) {
		sIP5060ProvisioningRequest.delete(db);
	}
	
	*/
}
