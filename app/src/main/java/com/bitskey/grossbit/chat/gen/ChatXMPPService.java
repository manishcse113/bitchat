package com.bitskey.grossbit.chat.gen;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.chat.ChatBubbleActivity;
import com.bitskey.grossbit.chat.db.ChatRecord;
import com.bitskey.grossbit.chat.db.DataSource;
import com.bitskey.grossbit.chat.db.MyRecord;
import com.bitskey.grossbit.chat.db.UserRecord;
import com.bitskey.grossbit.chat.login.ChatConnection;
import com.bitskey.grossbit.chat.login.ConnectivityReceiver;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;


//import org.jivesoftware.smack.xevent.MessageEventManager;

import java.util.Collection;
import java.util.List;
import java.util.Map;

//import com.bitskey.grossbit.chat.chat2.MainActivity;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class ChatXMPPService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_LOGIN = "com.bitskey.grossbit.chat.action.LOGIN";
    private static final String ACTION_REGISTER = "com.bitskey.grossbit.chat.action.REGISTER";

    public static final String ACTION_USERLIST = "com.bitskey.grossbit.chat.action.USERLIST";
    public static final String ACTION_USERLIST_PRESENCE_OFF = "com.bitskey.grossbit.chat.action.USERLIST_PRESENCE_OFF";
    public static final String ACTION_USERLIST_PRESENCE_ON = "com.bitskey.grossbit.chat.action.USERLIST_PRESENCE_ON";
    public static final String ACTION_USERLIST_PRESENCE_AWAY = "com.bitskey.grossbit.chat.action.USERLIST_PRESENCE_AWAY";

    public static final String ACTION_SENDMESSAGE = "com.bitskey.grossbit.chat.action.SENDMESSAGE";
    public static final String ACTION_RECEIVEDMESSAGE = "com.bitskey.grossbit.chat.action.RECEIVEDMESSAGE";
    public static final String ACTION_RECEIVEDIMAGE = "com.bitskey.grossbit.chat.action.RECEIVEDIMAGE";

    public static final String HOSTIP = "54.148.100.240";
    public static final String CHAT_MESSAGETYPE = "com.bitskey.grossbit.chat.action.MESSAGETYPE";
    public static final String CHAT_TIMESTAMP = "com.bitskey.grossbit.chat.action.TIMESTAMP";
    public static final String AWAY = "away";
    public static final String ONLINE = "online";
    public static final String OFFLINE = "offline";

    public static String DOMAIN = "ec2-54-148-100-240.us-west-2.compute.amazonaws.com";

    //TBD: Following to be corrected later- this is done, as we are testing sometimes with spark and sometimes with our application
    public static String DOMAINJID = "ec2-54-148-100-240.us-west-2.compute.amazonaws.com/Spark 2.7.0";//

    // TODO: Rename parameters
    private static final String USER_NAME = "com.bitskey.grossbit.chat.extra.PARAM1";
    private static final String PASSWORD = "com.bitskey.grossbit.chat.extra.PARAM2";
    private static final String LOGGEDIN = "com.bitskey.grossbit.chat.extra.PARAM3";

    public static final String STR_LOGGEDIN = "LOGGEDIN";

    private static final String MESSAGETOSEND = "com.bitskey.grossbit.chat.extra.PARAM4";
    private static final String TOUSER = "com.bitskey.grossbit.chat.extra.PARAM5";

    public static final String MESSAGERECEIVED = "com.bitskey.grossbit.chat.extra.PARAM6";

    public static final String FILESIZEEXPECTED = "com.bitskey.grossbit.chat.extra.PARAM7";
    public static final String FILESIZEDISK = "com.bitskey.grossbit.chat.extra.PARAM8";
    public static final String TIMESTAMP = "com.bitskey.grossbit.chat.extra.PARAM9";
    public static final String FILESENT = "com.bitskey.grossbit.chat.extra.PARAM10";
    public static final String MESSAGESENT = "com.bitskey.grossbit.chat.extra.PARAM11";
    public static final String RECEIPTID = "com.bitskey.grossbit.chat.extra.PARAM12";
    public static final String RECEIPTSTATUS = "com.bitskey.grossbit.chat.extra.PARAM13";
    public static final String MESSAGEDIRECTION = "com.bitskey.grossbit.chat.extra.PARAM14";

    /// messagener code: for send receive from service to activity
    /**
     * Message type: register the activity's messenger for receiving responses
     * from Service. We assume only one activity can be registered at one time.
     */
    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST = 1;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST = 2;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY = 3;
    public static final int INTER_TASK_MESSAGE_TYPE_USERLIST = 4;

    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN = 5;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN = 6;
    public static final int INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY = 7;

    public static final int INTER_TASK_MESSAGE_TYPE_REGISTER_CHAT = 8;
    public static final int INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT = 9;
    public static final int INTER_TASK_MESSAGE_TYPE_CHAT_QUERY = 10;

    public static final int INTER_TASK_MESSAGE_TYPE_MESSAGE = 11;

    public static final int INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT = 12;
    //public static final int MESSAGE_TYPE_MESSAGE_RECEIVEDTEXT = 12;
    //public static final int MESSAGE_TYPE_MESSAGE_RECEIVEDIMAGE = 13;


    public static boolean isViewIntentSafe = false;
    private final String FILENAME = "GBC ChatXMPPService#";
    private boolean monitoringThreadRunning = false;

    XMPPTCPConnection mConnection;
    Roster mRoster;
    static UserMaps mUsersListMap;
    public static String mUserName;
    String mPassword;

    ChatManagerListener myChatListener;
    static boolean SEND_ACTION_USERLIST;
    static boolean mFirstTimeLaunch;

    private int debugVar = 0;
    public static int count = 0;
    private NotificationManager mNM;
    public ChatXMPPService() {

        super("ChatXMPPService");
        SEND_ACTION_USERLIST = true;
        mFirstTimeLaunch = true;
    }
    @Override
    public int onStartCommand(Intent intent, int flag, int startId) {
        int ret = super.onStartCommand(intent, flag, startId);
        Log.d(FILENAME, "onStartCommand");
        debugVar++;
        return ret;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(FILENAME, "onCreate service ChatXMPPService + " + debugVar);
        //mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        // Display a notification about us starting.
        //showNotification();

        Intent intent = new Intent(Intent.ACTION_VIEW);
        PackageManager packageManager = getPackageManager();
        List activities = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        isViewIntentSafe = activities.size() > 0;
    }

    @Override
    public void onDestroy() {
        Log.d(FILENAME, "onDestroy service ChatXMPPService +" + debugVar);
        // Cancel the persistent notification.
        //mNM.cancel(R.string.remote_service_started);
        // Tell the user we stopped.
        //Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOGIN.equals(action)) {
                Log.d(FILENAME, "Received intent ACTION_LOGIN");
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                final String param3 = intent.getStringExtra(LOGGEDIN);
                handleActionLogin(param1, param2, param3);
                /*
                new Thread(new Runnable() {
                    public void run() {
                        ReceiveChatMessages();
                    }
                }).start();
                */
            } else if (ACTION_REGISTER.equals(action)) { // This is TODO TBD
                final String param1 = intent.getStringExtra(USER_NAME);
                final String param2 = intent.getStringExtra(PASSWORD);
                handleActionREGISTER(param1, param2);
            } else if (ACTION_SENDMESSAGE.equals(action)) {
                final String param1 = intent.getStringExtra(MESSAGETOSEND);
                final String param2 = intent.getStringExtra(TOUSER);
                final String param3 = intent.getStringExtra(CHAT_MESSAGETYPE);
                final String param4 = intent.getStringExtra(CHAT_TIMESTAMP);
                if (param3.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                    handleActionSendMessage(param1, param2, param4);
                }
                /*
                else if (param3.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_IMAGE)) {
                    new Thread(new Runnable() {
                        public void run() {
                            FileTransferWrapper.SendFile(param1, param2 + "@" + ChatXMPPService.DOMAIN, false);
                        }
                    }).start();
                }*/
                /*
                new Thread(new Runnable() {
                    public void run() {
                        handleActionSendMessage(param1, param2);
                    }
                }).start();*/
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionLogin(String USER_NAME, String PASSWORD, String loggedIn) {
        mUserName = USER_NAME;
        mPassword = PASSWORD;
        //invokConnectionXmpp();
        Log.d(FILENAME, "handleActionLogin");
        if (loggedIn.equals(STR_LOGGEDIN)) {
            prepareUsersList();// Only from roster: by default no user is in roster
            registerRosterListners();
            ReceiveChatMessages();
            FileTransferWrapper.ReceiveFile(this.getBaseContext());
        }
        else //offline mode
        {
            prepareUsersListFromDb();
            // As soon as network connection is up, it shall try to login from a thread,
            // on succes it shall do the remainder of task such as
            //registerRosterListners();
            //ReceiveChatMessages();
            //FileTransferWrapper.ReceiveFile(this.getBaseContext());
        }
        StartUserListActivity(ACTION_USERLIST);
        //Start thread only once to monitor the connection with the Server
        if (monitoringThreadRunning == false)
        {
            monitoringThreadRunning = true;
            monitoringConnectivityThread();
        }
    }

    private void monitoringConnectivityThread()
    {
        new Thread() {
            @Override
            public void run() {
                boolean isNetworkConnected= false;
                boolean isServerConnected = false;
                String userName = "";
                String password = "";
                //Retrieve username and password;
                DataSource db = new DataSource(getApplicationContext());
                db.open();
                List<MyRecord> reqs = db.getMyRecords();
                for(MyRecord _req : reqs) {
                    userName = _req.getUserName();
                    password = _req.getPassword();
                    Log.d(FILENAME, "username and password are " + userName + " " + password);
                }
                if (userName==null || password == null)
                {
                    Log.e(FILENAME, "ChatXmppService:monitoringConnectivityThread: Error - user name and password are null");
                    return;
                }
                while(true) //infinite monitoring thread:
                {
                    try
                    {

                        //Get the network state
                        isNetworkConnected = ConnectivityReceiver.isConnected(getBaseContext());
                        //Get the connection state
                        mConnection = ChatConnection.getInstance().getConnection();
                        if (mConnection!=null)
                        {
                            isServerConnected = mConnection.isConnected();
                        }
                        // if connectionState is down and network state is up - attempt login
                        if (isNetworkConnected == false)
                        {
                            Thread.sleep(5000L);
                        }
                        else if (isNetworkConnected == true && isServerConnected == false)
                        {
                            if (mConnection!=null) {
                                isServerConnected = ChatConnection.getInstance().connect(getApplicationContext(), userName, password);
                                if ( isServerConnected)
                                {
                                    // If login is successful - register for various listners - update users list:
                                    prepareUsersList();// Only from roster: by default no user is in roster
                                    registerRosterListners();
                                    ReceiveChatMessages();
                                    FileTransferWrapper.ReceiveFile(getApplicationContext());
                                }
                            }
                            Thread.sleep(2000L);
                        }
                        else //netowrk is connected, server is also connected
                        {
                            //Can check every 15 seconds
                            Thread.sleep(15000L);
                        }
                    }
                    catch (Exception e)
                    {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }

    public void prepareUsersList() {
        Log.d(FILENAME, " prepareUsersList");
        mConnection = ChatConnection.getInstance().getConnection();
        mRoster = Roster.getInstanceFor(mConnection);
        mRoster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
        Collection<RosterEntry> entries = mRoster.getEntries();
        if ( mUsersListMap == null) {
            mUsersListMap = new UserMaps();
        }
        int i = 0;
        for (RosterEntry entry : entries) {
            String name = entry.getName();//.substring(0, entry.getName().indexOf("@"));
            //String name = entry.substring(0, entry.indexOf("@"));
            Presence presence = null;
            if (name != null) {

                //mUsersListMap.mUserJidMap.put(name, jid);
                presence = mRoster.getPresence(name);
                String fullName = presence.getFrom();
                if (presence.isAvailable()) {
                    String jid = fullName.substring(fullName.indexOf("@"));
                    mUsersListMap.mUserJidMap.put(name, jid);
                }
            }
            /* to pass user and presence mapping*/
            UserRecord.storeUniqueNameRecord(this, name, null, null, null);//except name - everything is stored as null, presence is temporary and not stored.
            if ((name != null) && (presence.isAvailable())) {
                if (presence.isAway()) {
                    Log.d(FILENAME, " startUserListActivity name" + name + AWAY);
                    mUsersListMap.mUserPresenceMap.put(name, AWAY);
                } else {
                    Log.d(FILENAME, " startUserListActivity name" + name + ONLINE);
                    mUsersListMap.mUserPresenceMap.put(name, ONLINE);
                }
            } else {
                Log.d(FILENAME, " startUserListActivity name" + name + OFFLINE);
                mUsersListMap.mUserPresenceMap.put(name, OFFLINE);
            }
            Log.d(FILENAME, name);
        }
    }

    private void prepareUsersListFromDb()
    {
            if ( mUsersListMap == null) {
                mUsersListMap = new UserMaps();
            }
            UserRecord.clearFromDB(getApplicationContext(),mUserName);
            DataSource db = new DataSource(this);
            db.open();

            List<UserRecord> reqs = db.getUserRecords();
            for(UserRecord _req : reqs) {
                mUsersListMap.mUserPresenceMap.put(_req.getUserName(), OFFLINE);
            }
            db.close();
     }

    public void StartUserListActivity(String action) {
        Log.d (FILENAME, "StartUserListActivity");
        Intent intent = new Intent(getBaseContext(), ChatUsersListActivity.class);
        intent.setAction(action);//New message to be defined
        //if (mFirstTimeLaunch) {
        //    mFirstTimeLaunch = false;
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //}
        Bundle bundle = new Bundle();
        bundle.putSerializable("userPresenceMap", mUsersListMap);
        intent.putExtras(bundle);
        loguserlist();
        startActivity(intent);
    }


    void ReceiveChatMessages() {
        Log.d(FILENAME, " ReceiveChatMessages");
        ChatManager chatManager = null;
        chatManager = ChatManager.getInstanceFor(mConnection);
        final MyChatMessageListener myChatMessageListener = new MyChatMessageListener();
        myChatListener = new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(myChatMessageListener);
            }
        };
        chatManager.addChatListener(myChatListener);
        //FileTransferWrapper.FileReceive();
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionREGISTER(String USER_NAME, String PASSWORD) {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }

    public void registerRosterListners()
    {
        Log.d(FILENAME, "registerRosterListners");
        mConnection = ChatConnection.getInstance().getConnection();
        if (mConnection.isConnected()) {
            mRoster = Roster.getInstanceFor(mConnection);
            mRoster.setSubscriptionMode(Roster.SubscriptionMode.accept_all);
            mRoster.addRosterListener(new RosterListener() {
                public void entriesAdded(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesAdded");
                    int i = 0;
                    for (String entry : addresses) {
                        String name = entry.substring(0, entry.indexOf("@"));
                        mRoster = Roster.getInstanceFor(mConnection);
                        Presence presence = mRoster.getPresence(name);
                        String fullName = presence.getFrom();
                        UserRecord.storeUniqueNameRecord(getBaseContext(), name,null,null,null);//except name - everything is stored as null, presence is temporary and not stored.
                        if (presence.isAvailable()) {
                            //Seems it never comes here.....
                            String jid = fullName.substring(fullName.indexOf("@"));
                            mUsersListMap.mUserJidMap.put(name, jid);
                            if (presence.isAway()) {
                                mUsersListMap.mUserPresenceMap.put(name, AWAY);
                            } else {
                                mUsersListMap.mUserPresenceMap.put(name, ONLINE);
                            }
                        } else {
                            mUsersListMap.mUserPresenceMap.put(name, OFFLINE);
                        }
                        Log.d(FILENAME, name + "Presence " + presence.isAvailable());
                    }
                    SEND_ACTION_USERLIST = true;
                    //StartUserListActivity(ACTION_USERLIST);
                    if (mUsersListMap != null) {
                        SendtoUserListActivity(ACTION_USERLIST);
                    }
                }

                public void entriesDeleted(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesDeleted");
                    for (String entry : addresses) {
                        String name = entry.substring(0, entry.indexOf("@"));
                        Log.d(FILENAME, name);
                        mUsersListMap.mUserPresenceMap.remove(name);

                        String jid = entry.substring(entry.indexOf("/"));
                        mUsersListMap.mUserJidMap.remove(name);
                        Log.d(FILENAME, "user " + name + " removed ");
                    }
                    SEND_ACTION_USERLIST = true;
                    //StartUserListActivity(ACTION_USERLIST);
                    if (mUsersListMap != null) {
                        SendtoUserListActivity(ACTION_USERLIST);
                    }
                }

                public void entriesUpdated(Collection<String> addresses) {
                    Log.d(FILENAME, " entriesUpdated");
                    int i = 0;
                    for (String entry : addresses) {
                        String name = entry.substring(0, entry.indexOf("@"));
                        String presenceStr = OFFLINE;
                        mRoster = Roster.getInstanceFor(mConnection);
                        Presence presence = mRoster.getPresence(name);
                        String fullName = presence.getFrom();
                        if (presence.isAvailable()) {
                            String jid = fullName.substring(fullName.indexOf("@"));
                            mUsersListMap.mUserJidMap.put(name, jid);

                            if (presence.isAway()) {
                                Log.d(FILENAME, " entriesUpdated name" + name + AWAY);
                                mUsersListMap.mUserPresenceMap.put(name, AWAY);
                            } else {
                                Log.d(FILENAME, " entriesUpdated name" + name + ONLINE);
                                mUsersListMap.mUserPresenceMap.put(name, ONLINE);
                            }
                        } else {
                            Log.d(FILENAME, " entriesUpdated name" + name + OFFLINE);
                            mUsersListMap.mUserPresenceMap.put(name, OFFLINE);
                        }
                        Log.d(FILENAME, name);
                    }
                    SEND_ACTION_USERLIST = true;
                    //StartUserListActivity(ACTION_USERLIST);
                    if (mUsersListMap != null) {
                        SendtoUserListActivity(ACTION_USERLIST);
                    }
                }

                public void presenceChanged(Presence presence) {
                    Log.d(FILENAME, " presenceChanged");
                    String fullName = presence.getFrom();
                    String name = fullName.substring(0, fullName.indexOf("@"));
                    UserRecord.storeUniqueNameRecord(getApplicationContext(), name,null,null,null);//except name - everything is stored as null, presence is temporary and not stored.
                    if (presence.isAvailable()) {
                        String jid = fullName.substring(fullName.indexOf("@"));
                        mUsersListMap.mUserJidMap.put(name, jid);

                        if (presence.isAway()) {
                            Log.d(FILENAME, " presenceChanged name" + name + AWAY);
                            mUsersListMap.mUserPresenceMap.put(name, AWAY);
                        } else {
                            Log.d(FILENAME, " presenceChanged name" + name + ONLINE);
                            mUsersListMap.mUserPresenceMap.put(name, ONLINE);
                        }
                    } else {
                        Log.d(FILENAME, " presenceChanged name" + name + OFFLINE);
                        mUsersListMap.mUserPresenceMap.put(name, OFFLINE);
                    }
                    Log.d(FILENAME, name);
                    SEND_ACTION_USERLIST = true;
                    //StartUserListActivity(ACTION_USERLIST);
                    if (mUsersListMap != null) {
                        SendtoUserListActivity(ACTION_USERLIST);
                    }
                }
            });
        }
    }

    void handleActionSendMessage(String message, String toUser, String timeStamp) {
        ChatManager chatManager = null;
        try {
            if (mConnection.isConnected()) {
                chatManager = ChatManager.getInstanceFor(mConnection);
                Chat chat3 = chatManager.createChat(toUser + "@" + DOMAIN);
                //Chat chat3 = chatManager.createChat(toUser);
                try {;
                    Message message2 = new Message(message);
                    //message2.p

                    //MessageEventManager.addNotificationsRequests(message, true, true, true, true);
                    chat3.sendMessage(message);
                    String receiptId = message2.getStanzaId();
                    String receiptStatus = ChatBubbleActivity.MESSAGE_ACK_AWAITED;
                    ChatRecord.storeMessage(this, ChatXMPPService.mUserName, toUser, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT, message, null, timeStamp, receiptId, receiptStatus, 0, 0);
                    //update receiptId as well in db.

                } catch (SmackException.NotConnectedException e) {
                    String string = e.getMessage();
                    Log.e(FILENAME,"Exception" + string);
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            String string = e.getMessage();
            Log.e(FILENAME,"Exception" + string);
            e.printStackTrace();
        }
    }
    class MyChatMessageListener implements ChatMessageListener {
        @Override
        public void processMessage(Chat chat, Message message) {
            Log.d(FILENAME, "MyChatMessageListener: processMessage:" + message.getBody());
            String from = message.getFrom();
            String body = message.getBody();
            if (body == null) {
                return;
            }
            String remoteUserName = from.substring(0, from.indexOf("@"));
            /*String screenText = remoteUserName;
            screenText += ": ";
            screenText += body;
            screenText += "\n";
            */
            String screenText = body;
            //informChatActivity(remoteUserName, screenText, body);
            SendtoChatActivity(remoteUserName, screenText);
        }

    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());
    public static Messenger mResponseMessengerUserList = null;
    public static Messenger mResponseMessengerChat = null;
    public static  Messenger mResponseMessengerLogin = null;

    @Override
    public IBinder onBind(Intent intent) {
        super.onBind(intent);
        debugVar += 2;
        Log.d(FILENAME, "onBind Binding messenger..." + debugVar);
        return mMessenger.getBinder();
    }

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                //following case not used currently
                case INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY:
                    Bundle b = msg.getData();
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }
                    break;
                case INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST:
                    Log.d(FILENAME, "Registered USERLIST Activity's Messenger.");
                    mResponseMessengerUserList = msg.replyTo;
                    SendtoUserListActivity(ACTION_USERLIST);
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST:
                    Log.d(FILENAME, "UnRegistered USERLIST Activity's Messenger.");
                    mResponseMessengerUserList = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN:
                    Log.d(FILENAME, "Registered LOGIN Activity's Messenger.");
                    mResponseMessengerLogin = msg.replyTo;
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN:
                    Log.d(FILENAME, "UnRegistered LOGIN Activity's Messenger.");
                    mResponseMessengerLogin = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY:
                    b = msg.getData();
                    /*
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }*/
                    break;
                case INTER_TASK_MESSAGE_TYPE_REGISTER_CHAT:
                    Log.d(FILENAME, "Registered CHAT Activity's Messenger.");
                    mResponseMessengerChat = msg.replyTo;
                    break;
                case INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT:
                    Log.d(FILENAME, "UnRegistered CHAT Activity's Messenger.");
                    mResponseMessengerChat = null;
                    break;
                case INTER_TASK_MESSAGE_TYPE_CHAT_QUERY:
                    b = msg.getData();
                    /*
                    if (SEND_ACTION_USERLIST == true) {
                        if (b != null) {
                            SendtoUserListActivity(ACTION_USERLIST);
                        } else {
                            //sendToActivity("Who's there? Speak!");
                        }
                        SEND_ACTION_USERLIST = false;
                    }*/
                    break;

                default:
                    super.handleMessage(msg);
            }
        }
    }

    void SendtoUserListActivity(String action) {
        if (mResponseMessengerUserList == null) {
            Log.d(FILENAME, "SendtoUserListActivity: Cannot send message to activity - no activity registered to this service.");
        } else {
            Log.d(FILENAME, "Sending message to USERLIST activity: " + action);
            Bundle data = new Bundle();
            data.putSerializable("userPresenceMap", mUsersListMap);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_USERLIST);
            msg.setData(data);
            try {
                mResponseMessengerUserList.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "SendtoUserListActivity Error" + e.getMessage());
                e.printStackTrace();
            }
            loguserlist();
        }
    }

    void SendtoChatActivity(String remoteUserName, String screenText) {
        if (mResponseMessengerChat == null) {
            Log.d(FILENAME, "SendtoChatActivity: Cannot send message to activity - no activity registered to this service. So showing notification intent");
            count++;

            String timeStamp = TimeClock.getTimeStamp();
            String receiptId = timeStamp;
            //Store message here.
            ChatRecord.storeMessage(this, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONRECEIVED, null, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT, screenText, null, timeStamp, receiptId, ChatBubbleActivity.MESSAGE_ACKED, 0, 0);
            //Notification action is just to inform user of messages:
            prepareAndSendNotification(this, remoteUserName, screenText, false);
        } else {
            Log.d(FILENAME, "Sending message to CHAT activity: " + remoteUserName + " text: " + screenText);
            count = 0;
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT);
                data.putSerializable(ChatXMPPService.MESSAGERECEIVED, screenText);
            android.os.Message msg = android.os.Message.obtain(null, INTER_TASK_MESSAGE_TYPE_MESSAGE);
            msg.setData(data);
            try {
                mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME,"SendtoChatActivity" +e.getMessage() );
                e.printStackTrace();
            }
        }
    }

    public static void prepareAndSendNotification(Context context, String remoteUserName, String screenText, boolean isFile)
    {
        //Context context = getApplicationContext();
        Intent resultIntent = new Intent(context, ChatBubbleActivity.class);
        resultIntent.setAction(ChatXMPPService.ACTION_RECEIVEDMESSAGE);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
        resultIntent.putExtra(ChatXMPPService.MESSAGERECEIVED, screenText);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack
        stackBuilder.addParentStack(ChatBubbleActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (isFile)
        {
            screenText = screenText.substring(screenText.lastIndexOf("/")+1);
        }
        mNotificationManager.notify(10, builder.setContentTitle("GrossBitChat from " + remoteUserName)
                .setContentText(screenText)
                .setSmallIcon(R.drawable.icon23)
                .setVibrate(new long[]{1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .setTicker("bitskey")
                .setNumber(count)
                .build());
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionLogin(Context context, String param1, String param2, String loggedIn) {
        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_LOGIN);
        intent.putExtra(USER_NAME, param1);
        intent.putExtra(PASSWORD, param2);
        intent.putExtra(LOGGEDIN, loggedIn);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionREGISTER(Context context, String param1, String param2) {
        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_REGISTER);
        intent.putExtra(USER_NAME, param1);
        intent.putExtra(PASSWORD, param2);
        context.startService(intent);
    }

    // TODO: Customize helper method
    public static void startActionSendMessage(Context context, String param1, String param2, String param3, String param4) {
        Intent intent = new Intent(context, ChatXMPPService.class);
        intent.setAction(ACTION_SENDMESSAGE);
        intent.putExtra(MESSAGETOSEND, param1);
        intent.putExtra(TOUSER, param2);
        intent.putExtra(CHAT_MESSAGETYPE, param3);
        intent.putExtra(CHAT_TIMESTAMP, param4);
        context.startService(intent);
    }

    void loguserlist() {
        if (mUsersListMap == null) {
            return;
        }
        for (Map.Entry<String, String> searchentry : mUsersListMap.mUserPresenceMap.entrySet()) {
            Log.d(FILENAME, " loguserlist user " + searchentry.getKey() + "presence " + searchentry.getValue());
        }
    }
}