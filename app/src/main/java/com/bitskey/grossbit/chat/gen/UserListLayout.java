package com.bitskey.grossbit.chat.gen;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.chat.ChatBubbleActivity;

import java.util.Map;

/**
 * Created by GUR11219 on 31-07-2015.
 */

public class UserListLayout extends ArrayAdapter<String> {
    int groupid;
    String[] userNamelist;
    Integer[] avatarList;
    Integer[] imageList;
    String [] avatarcontextList;
    Context context;
    //int Listsize = 0;
    int mapSize = 0;

    private final String FILENAME = "GBC UserListLaout#";

    private UserListCallback callback;

    public UserListLayout(Context context, int vg, int id, String[] ulist, Integer[] images, UserMaps listobj){
        super(context,vg, ulist );
        this.context=context;
        groupid=vg;

        mapSize = listobj.mUserPresenceMap.size();
        Log.d(FILENAME, "UserListLayout with size of Map#: "+ mapSize);
        Map<String, String> map = listobj.mUserPresenceMap;

        int i = 0;
        userNamelist = new String[map.size()];
        imageList = new Integer[map.size()];
        avatarList = new Integer [map.size()];
        avatarcontextList = new String[map.size()];
        for (Map.Entry<String, String> entry : map.entrySet()) {
            userNamelist[i] = entry.getKey();
            avatarList[i] = R.drawable.avatar;
            avatarcontextList[i] = "...";
            if (entry.getValue().equals("offline"))
            {
                imageList[i]=    R.drawable.offline;
            }
            else if (entry.getValue().equals("online"))
            {
                imageList[i]=    R.drawable.available;
            }
            else
            {
                imageList[i]=    R.drawable.away;
            }
            Log.d(FILENAME, "UserListLayout username "+ userNamelist[i] + " Presence " + imageList[i]);
            i++;
        }

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       // View itemView = inflater.inflate(groupid, parent, false);
        View activity_chatusers_list_view = inflater.inflate(R.layout.activity_chatusers_list, null);
        Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row ==null) {
            inflateUserListLayout(inflater, activity_chatusers_list_view, position );
      }
        else
        {
            //You get here in case of scroll
            //Currently both the behaviours are defined as same. But it might need to be revisited later.
            inflateUserListLayout(inflater, activity_chatusers_list_view, position );
        }
        return activity_chatusers_list_view;
    }

    public void inflateUserListLayout(LayoutInflater inflater, View activity_chatusers_list_view, final int position)
    {
        try {
            View user_view = inflater.inflate(R.layout.userlistlayout, (ViewGroup) activity_chatusers_list_view, false);

            ImageView imageView = (ImageView) user_view.findViewById(R.id.icon);
            imageView.setImageDrawable(context.getResources().getDrawable(imageList[position]));

            ImageView avatarView = (ImageView) user_view.findViewById(R.id.avatar);
            avatarView.setImageDrawable(context.getResources().getDrawable(avatarList[position]));

            TextView textuserName = (TextView) user_view.findViewById(R.id.username);
            textuserName.setText(userNamelist[position]);
            textuserName.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        if ( ChatUsersListActivity.mIsFileSelectedForSharing == true) //Case: where intent received from external source for file sharing.
                        {
                            startActionChatExternalFileShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                            ChatUsersListActivity.mIsFileSelectedForSharing = false;
                        }
                        else if (ChatUsersListActivity.mIsTextSelectedForSharing)//Case: where intent received from external Text source for file sharing.
                        {
                            startActionChatExternalTextShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                            ChatUsersListActivity.mIsTextSelectedForSharing = false;
                        }
                        else if( ChatBubbleActivity.mIsFileSelectedForSharing == true) //Case: where file being shared from user1 to user2
                        {
                            startActionChatInternalFileShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                        }
                        else if( ChatBubbleActivity.mIsTextSelectedForSharing == true) //Case: where file being shared from user1 to user2
                        {
                            startActionChatInternalTextShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                        }
                        else { //normal display of chat interface
                            startActionChat(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                        }
                        Toast.makeText(getContext(), "Clicked ", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            );

            /*
            TextView userContext = (TextView) user_view.findViewById(R.id.misc);
             //Commented following clear db, as it will be moved to context menu:
            userContext.setText(avatarcontextList[position]);
            userContext.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {

                        //startActionChat(ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                        //Toast.makeText(getContext(), "Clicked can clear db ", Toast.LENGTH_SHORT).show();
                        //ChatRecord.clearFromDB(context, ChatXMPPService.mUserName.toString(), userNamelist[position], false);
                        return;
                    }
                }
            );
            */

            ((ViewGroup) activity_chatusers_list_view).addView(user_view);
        }catch (Exception e)
        {
            Log.d(FILENAME, " Exception " + e.getMessage());
        }

    }
    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChat(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        context.startActivity(intent);
    }

    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatExternalFileShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_FILE_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        intent.putExtra(ChatUsersListActivity.EXTERNAL_FILE, ChatUsersListActivity.mfileNameWithPath);
        intent.putExtra(ChatUsersListActivity.MIME_TYPE, ChatUsersListActivity.mMimeType);
        context.startActivity(intent);
    }

    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatInternalFileShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_FILE_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        context.startActivity(intent);
    }


    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatExternalTextShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_TEXT_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        intent.putExtra(ChatUsersListActivity.EXTERNAL_TEXT, ChatUsersListActivity.mTextToSend);
        context.startActivity(intent);
    }
    public static void startActionChatInternalTextShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_TEXT_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        context.startActivity(intent);
    }

    public void setCallback(UserListCallback callback){
        this.callback = callback;
    }
    public interface UserListCallback {
        public void userSelected(int position, String selectedUserName);
        public void userDeSelected(int position, String selectedUserName);
        public void activateDefaultMenu();
        public void activateSelectedMenu();
    }


}
