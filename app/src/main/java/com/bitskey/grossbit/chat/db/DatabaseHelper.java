package com.bitskey.grossbit.chat.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.logging.Logger;

//Manish
public class DatabaseHelper extends SQLiteOpenHelper {
	
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private final static String DB_NAME = "GrossBitChat";
	private final static int DB_VERSION = 6;
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

		ChatRecord.onCreate(db);
		MyRecord.onCreate(db);
		UserRecord.onCreate(db);
		/*
		SIPIdentity.onCreate(db);
		PTTChannel.onCreate(db);
		ENUMSuffix.onCreate(db);
		
		//Manish: added for Ctalk, commented SIP5060
		CTalkRegisterRequest.onCreate(db);
		SIP5060ProvisioningRequest.onCreate(db);
		*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		logger.info("Upgrading DB from version " + oldVersion + " to version " + newVersion);

		ChatRecord.onUpgrade(db, oldVersion, newVersion);
		MyRecord.onUpgrade(db, oldVersion, newVersion);
		UserRecord.onUpgrade(db, oldVersion, newVersion);

	}
}
