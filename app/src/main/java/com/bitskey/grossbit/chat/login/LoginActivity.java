package com.bitskey.grossbit.chat.login;

/**
 * Created by GUR11219 on 02-07-2015.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.db.DataSource;
import com.bitskey.grossbit.chat.db.MyRecord;
import com.bitskey.grossbit.chat.gen.ChatXMPPService;
import com.bitskey.grossbit.chat.gen.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * Login Activity Class
 */
public class LoginActivity extends Activity {
        //extends Activity {
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    // Email Edit View Object
    EditText emailET;
    // Passwprd Edit View Object
    EditText pwdET;

    public static boolean autologin = false;
    String mUserName;
    String mPassword;

    //AbstractXMPPConnection mConnection = null;
    private final String FILENAME = "GBC LoginActivity#";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        Log.d(FILENAME, "onCreate");
        // Find Error Msg Text View control by ID
        errorMsg = (TextView) findViewById(R.id.login_error);
        // Find Email Edit View control by ID
        emailET = (EditText) findViewById(R.id.loginEmail);
        // Find Password Edit View control by ID
        pwdET = (EditText) findViewById(R.id.loginPassword);
        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
        mUserName = null;
        mPassword = null;

        //TBD: Auto login: Temporairy:
        //emailET.setText("manish");
        //pwdET.setText("nswdel10");

        //Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(myToolbar);

        doBindService();
        //autoLogin functionality to be done:
        //read DB, login automatically - if record is found:
        autoLogin();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(FILENAME, "onDestroy");
        finishActivity(0);
        //doUnbindService();
    }

    /**
     * Method gets triggered when Login button is clicked
     *
     * @param view
     */
    public void loginUser(View view) {

        Log.d(FILENAME, "loginUser");
        // Get Email Edit View Value
        String email = emailET.getText().toString();
        mUserName = email;
        // Get Password Edit View Value
        mPassword = pwdET.getText().toString();

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // When Email Edit View and Password Edit View have values other than Null
        if (Utility.isNotNull(email) && Utility.isNotNull(mPassword)) {
            // When Email entered is Valid
            //if(Utility.validate(email)){
            // Put Http parameter username with value of Email Edit View control
            //params.put("username", email);
            // Put Http parameter password with value of Password Edit Value control
            //params.put("password", mPassword);
            // Invoke RESTful Web Service with Http parameters
            new LoginBackground(LoginActivity.this).execute();
            //ChatXMPPService.startActionLogin(this, mUserName, mPassword );

        } else {
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }
    }


    //////////////// Code below this is not used for now ////////////////
    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoHomeActivity() {
        Log.d(FILENAME, "navigatetoHomeActivity");
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void navigatetoRegisterActivity(View view) {
        Log.d(FILENAME, "navigatetoRegisterActivity");
        Intent loginIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param
     */
    //public void getConnections(RequestParams params) {
    public void getUsers() {
        // Show Progress Dialog
        //prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        Log.d(FILENAME, "getUsers");
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        client.addHeader("Accept", "application/json");
        client.get("http://" + ChatXMPPService.DOMAIN + "/plugins/restapi/v1/users", new AsyncHttpResponseHandler() {
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                String str = new String(responseBody);
                //str = responseBody;
                Log.d("HTTP", "onSuccess: " + str);
                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Toast.makeText(getApplicationContext(), "No records found!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(str);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        Object value = obj.get(key);
                        Log.d("HTTP", "onSuccess" + value);
                        /*
                        try {
                            Object value = obj.get(key);
                            //JSONObject userObj = new JSONObject(value);
                            Iterator<String> iter2 = value.keys();
                            while (iter2.hasNext()) {
                                String key2 = iter2.next();
                                Object userRecord = userObj.get(key2);
                                Toast.makeText(getApplicationContext(), key2.toString() + userRecord.toString(), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                        */
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable error) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d("HTTP", "onFailure" + statusCode);

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    Toast.makeText(getApplicationContext(), "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    /**
     * Messenger used for communicating with service.
     */
    Messenger mService = null;
    boolean mServiceConnected = false;
    /**
     * Class for interacting with the main interface of the service.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            /*
            if (msg.what == ChatXMPPService.MESSAGE_TYPE_USERLIST) {
                Log.d(FILENAME,"IncomingHandler : MESSAGE_TYPE_USERLIST " );
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d("MessengerActivity", "Response: " + text);
            } else {
                super.handleMessage(msg);
            }
            */
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mServiceConnected = true;
            // Register our messenger also on Service side:
            Message msg = Message.obtain(null,
                    ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN);
            msg.replyTo = mMessenger;
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        }

        /**
         * Connection dropped.
         */
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(FILENAME, "Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }
    };
    /**
     * Sends message with text stored in bundle extra data ("data" key).
     *
     * @param code to send
     */
    void sendToService(int code) {
        if (mServiceConnected) {
            Message msg = null;
            if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_UNREGISTER_LOGIN");
                //Manish: Send from here, whatever to send to service from activity:
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            } else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_LOGIN_QUERY");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        } else {
            Log.d(FILENAME, "Cannot send - not connected to service.");
        }
    }


    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.d(FILENAME, "binding initiated");
        mIsBound = getApplicationContext().bindService(new Intent(getApplicationContext(), ChatXMPPService.class), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "binding done");
        //mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            Log.d(FILENAME, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN);
            }
            // Detach our existing connection.
            unbindService(mConn);
            mIsBound = false;
        }
    }

    //db is expected to have only one record.
    //If record is present then attempt-auto login on the record.
    protected void autoLogin()
    {
        DataSource db = new DataSource(this);
        db.open();
        List<MyRecord> reqs = db.getMyRecords();
        autologin = false;
        String userName = null;
        String password = null;
        for(MyRecord _req : reqs) {
            userName = _req.getUserName();
            password = _req.getPassword();
            autologin = true;
        }
        db.close();
        if ( autologin == true)
        {
            emailET.setText(userName);
            pwdET.setText(password);
            loginUser(getCurrentFocus());
        }
    }

}