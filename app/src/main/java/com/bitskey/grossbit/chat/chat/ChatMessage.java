package com.bitskey.grossbit.chat.chat;

import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 * Created by GUR11219 on 28-09-2015.
 */

public class ChatMessage extends Object {
    public int direction;
    public String message;
    //public String messagetype;
    public String filePath;
    public String fileName;
    public String mimeType;
    public String timeStamp;
    public String receiptId;
    public String receiptStatus;
    public long fileSizeExpected;
    public long fileSizeDisk;
    public String strFileSizeDisk;
    private final long KB = 1024;
    private final long MB = 1048576;
    private final long GB = 1073741824;


    public ChatMessage(int direction, String message, String mimeType, String filePath, String timeStamp, String receiptId, String receiptStatus, long fileSizeExpected, long fileSizeDisk) {
        super();
        this.direction = direction;
        this.message = message;
        //this.messagetype = messagetype;
        this.filePath = filePath;
        this.mimeType = mimeType;
        if (filePath!=null)
        {
            //get file name;
            fileName = filePath.substring(filePath.lastIndexOf("/")+1);
        }
        this.timeStamp = timeStamp;
        this.receiptId = receiptId;
        this.receiptStatus = receiptStatus;
        this.fileSizeExpected = fileSizeExpected;
        this.fileSizeDisk = fileSizeDisk;

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);


        strFileSizeDisk = "";

        if ( this.fileSizeExpected < KB)
        {
            strFileSizeDisk = String.valueOf(this.fileSizeDisk)  + " B";
        }
        else if ((this.fileSizeExpected >= KB) && (this.fileSizeExpected < MB))//Size in KB
        {
            strFileSizeDisk = String.valueOf(
                    (new Double(df.format(((double)(this.fileSizeExpected)) /KB)))) + " KB";
        }
        else if ((this.fileSizeExpected >= MB) && (this.fileSizeExpected < GB))//Size in MB
        {
            strFileSizeDisk = String.valueOf((new Double(df.format(((double)(this.fileSizeExpected))/MB)))) + " MB";
        }
        else //size is in GB
        {
            strFileSizeDisk = String.valueOf((new Double(df.format(((double)(this.fileSizeExpected))/GB)))) + " GB";
        }
    }
}