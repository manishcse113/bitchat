package com.bitskey.grossbit.chat.login;

import android.content.Context;
import android.util.Log;

import com.bitskey.grossbit.chat.gen.ChatXMPPService;
import com.bitskey.grossbit.chat.gen.UserMaps;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.SmackConfiguration;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;
import org.jivesoftware.smackx.address.provider.MultipleAddressesProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.CloseIQProvider;
import org.jivesoftware.smackx.bytestreams.ibb.provider.OpenIQProvider;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.chatstates.packet.ChatStateExtension;
import org.jivesoftware.smackx.commands.provider.AdHocCommandDataProvider;
import org.jivesoftware.smackx.delay.provider.LegacyDelayInformationProvider;
import org.jivesoftware.smackx.disco.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.disco.provider.DiscoverItemsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.iqlast.packet.LastActivity;
import org.jivesoftware.smackx.iqprivate.PrivateDataManager;
import org.jivesoftware.smackx.muc.packet.GroupChatInvitation;
import org.jivesoftware.smackx.muc.provider.MUCAdminProvider;
import org.jivesoftware.smackx.muc.provider.MUCOwnerProvider;
import org.jivesoftware.smackx.muc.provider.MUCUserProvider;
import org.jivesoftware.smackx.offline.packet.OfflineMessageInfo;
import org.jivesoftware.smackx.offline.packet.OfflineMessageRequest;
import org.jivesoftware.smackx.privacy.provider.PrivacyProvider;
import org.jivesoftware.smackx.receipts.DeliveryReceiptManager;
import org.jivesoftware.smackx.receipts.ReceiptReceivedListener;
import org.jivesoftware.smackx.search.UserSearch;
import org.jivesoftware.smackx.sharedgroups.packet.SharedGroupsInfo;
import org.jivesoftware.smackx.si.provider.StreamInitiationProvider;
import org.jivesoftware.smackx.vcardtemp.provider.VCardProvider;
import org.jivesoftware.smackx.xdata.provider.DataFormProvider;
import org.jivesoftware.smackx.xhtmlim.provider.XHTMLExtensionProvider;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

//import org.jivesoftware.smackx.IBBProvider.*;

/**
 * Created by GUR11219 on 22-09-2015.
 */
public class ChatConnection {

    private final String FILENAME = "GBC ChatConnectio#";
    static XMPPTCPConnection mConnection;
    Roster mRoster;
    UserMaps mUsersListMap;
    String mUserName;
    String mPassword;
    public int MAXCONNECTRETRIES = 1;
    static Context context;

    public static enum Status {
        FINISHED, PENDING, RUNNING;
    }

    Status status;
    private static ChatConnection ourInstance = new ChatConnection();

    public static ChatConnection getInstance() {
        return ourInstance;
    }

    private ChatConnection() {
        status = Status.PENDING;
    }

    public Status getStatus()
    {
        return status;
    }

    public boolean connect(final Context context, String userName, String password)
    {
        mConnection = null;
        status = Status.RUNNING;
        mUserName = userName;
        mPassword = password;
        this.context = context;
        Log.d(FILENAME, "connect");
        SmackConfiguration.DEBUG = true;
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        Log.d(FILENAME, "configBuilder takes time");
        //Enabled to see the debugging message of xml in logcat
        //configBuilder.setDebuggerEnabled(true);
        configBuilder.setUsernameAndPassword(mUserName, mPassword);

        //TBD: Following to be moved to config file --later
        //Manish:: TBD: Trying to login with resourceId
        //configBuilder.setServiceName(DOMAINJID);
        configBuilder.setServiceName(ChatXMPPService.DOMAIN);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.ifpossible);
        configBuilder.setHost(ChatXMPPService.HOSTIP);
        Log.d(FILENAME, "Trying Login: User: " + mUserName + " Password: " + mPassword);
        try {
            TLSUtils.acceptAllCertificates(configBuilder);
        } catch (Exception ex) {
            String message = ex.getMessage();
            Log.e(FILENAME, message);
        }
        configBuilder.setHostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        configure(new ProviderManager());

        mConnection = new XMPPTCPConnection(configBuilder.build());

        //Make max 4 retries in case of failure
        boolean connected = false;
        int retrycount = 0;
        while(connected==false && retrycount++ < MAXCONNECTRETRIES) {
            // Connect to the server
            try {
                mConnection.connect();
                mConnection.login();
                connected = true;
            } catch (Exception ex) {
                String msg = ex.getMessage();
                Log.e(FILENAME + "Error", " Connection try#" + retrycount + "failed::" + msg);
                /*
                if (msg.equals("Client is already connected"))
                {
                    connected = true;
                    break;
                }
                */
            }
        }
        if (connected==true) {
            Log.d(FILENAME, " Login successful");
            status = Status.FINISHED;

            DeliveryReceiptManager dm = DeliveryReceiptManager.getInstanceFor(mConnection);
            dm.setDefaultAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);
            dm.autoAddDeliveryReceiptRequests();
            dm.setAutoReceiptMode(DeliveryReceiptManager.AutoReceiptMode.always);

            //dm.enableAutoreceipts();
            dm.addReceiptReceivedListener(new ReceiptReceivedListener() {
                public void onReceiptReceived(String fromJid, String toJid, String receiptId, Stanza receipt) {
                    // If the receiving entity does not support delivery receipts,
                    // then the receipt received listener may not get invoked.
                    //changeMessageDeliveryStatus(receiptId, ChatConstants.DS_ACKED);
                    //get from userName
                    String name = fromJid.substring(0, fromJid.indexOf("@"));
                    //get to RemoteuserName
                    String remoteUsername = toJid.substring(0, toJid.indexOf("@"));
                    //FileTransferWrapper.updateChatRecordRequest(context, ChatXMPPService.mUserName, remoteUsername, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
                    //call loadAndUpdate:
                    //ChatRecrd.LoadAndUpdateChatRecord(context, name, remoteUsername, false, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
                }
            });
        }
        else{
            Log.d(FILENAME, " Login failed for times #" + MAXCONNECTRETRIES + "Check Server or connection and Retry login again");
            status = Status.PENDING;
        }
        return connected;
    }

    public XMPPTCPConnection getConnection()
    {
        return mConnection;
    }

    public void configure(ProviderManager pm) {
        // Private Data Storage
        pm.addIQProvider("query", "jabber:iq:private",
                new PrivateDataManager.PrivateDataIQProvider());
        // Time
        try {
            pm.addIQProvider("query", "jabber:iq:time",
                    Class.forName("org.jivesoftware.smackx.packet.Time"));
        } catch (ClassNotFoundException e) {
            Log.w("TestClient",
                    "Can't load class for org.jivesoftware.smackx.packet.Time");
        }
        /*
        // Roster Exchange
        pm.addExtensionProvider("x", "jabber:x:roster",
                new RosterExchangeProvider());
        // Message Events
        pm.addExtensionProvider("x", "jabber:x:event",
                new MessageEventProvider());
        */
        // Chat State
        pm.addExtensionProvider("active",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("composing",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("paused",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("inactive",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        pm.addExtensionProvider("gone",
                "http://jabber.org/protocol/chatstates",
                new ChatStateExtension.Provider());
        // XHTML
        pm.addExtensionProvider("html", "http://jabber.org/protocol/xhtml-im",
                new XHTMLExtensionProvider());
        // Group Chat Invitations
        pm.addExtensionProvider("x", "jabber:x:conference",
                new GroupChatInvitation.Provider());
        // Service Discovery # Items
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#items",
                new DiscoverItemsProvider());
        // Service Discovery # Info
        pm.addIQProvider("query", "http://jabber.org/protocol/disco#info",
                new DiscoverInfoProvider());
        // Data Forms
        pm.addExtensionProvider("x", "jabber:x:data", new DataFormProvider());
        // MUC User
        pm.addExtensionProvider("x", "http://jabber.org/protocol/muc#user",
                new MUCUserProvider());
        // MUC Admin
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#admin",
                new MUCAdminProvider());
        // MUC Owner
        pm.addIQProvider("query", "http://jabber.org/protocol/muc#owner",
                new MUCOwnerProvider());
        /* Following was causing random error in parsing date and thus connection was getting closed sometimes: Manish. Next line is alternate solution to this
        // Delayed Delivery
        pm.addExtensionProvider("x", "jabber:x:delay",
                new DelayInformationProvider());
                */
        pm.addExtensionProvider("x", "jabber:x:delay",
                new LegacyDelayInformationProvider());

        // Version
        try {
            pm.addIQProvider("query", "jabber:iq:version",
                    Class.forName("org.jivesoftware.smackx.packet.Version"));

        } catch (ClassNotFoundException e) {
            // Not sure what's happening here.
        }
        // VCard
        pm.addIQProvider("vCard", "vcard-temp", new VCardProvider());
        // Offline Message Requests
        pm.addIQProvider("offline", "http://jabber.org/protocol/offline",
                new OfflineMessageRequest.Provider());
        // Offline Message Indicator
        pm.addExtensionProvider("offline",
                "http://jabber.org/protocol/offline",
                new OfflineMessageInfo.Provider());
        // Last Activity
        pm.addIQProvider("query", "jabber:iq:last", new LastActivity.Provider());
        // User Search
        pm.addIQProvider("query", "jabber:iq:search", new UserSearch.Provider());
        // SharedGroupsInfo
        pm.addIQProvider("sharedgroup",
                "http://www.jivesoftware.org/protocol/sharedgroup",
                new SharedGroupsInfo.Provider());
        // JEP-33: Extended Stanza Addressing
        pm.addExtensionProvider("addresses",
                "http://jabber.org/protocol/address",
                new MultipleAddressesProvider());
        // FileTransfer
        pm.addIQProvider("si", "http://jabber.org/protocol/si",
                new StreamInitiationProvider());

        pm.addIQProvider("query", "http://jabber.org/protocol/bytestreams",
                new BytestreamsProvider());

        FileTransferNegotiator.IBB_ONLY = true;

        /* If enabled, it crashes login-seems a known issue with smack
        pm.addIQProvider("open", "http://jabber.org/protocol/ibb", new OpenIQProvider());
        pm.addIQProvider("close","http://jabber.org/protocol/ibb", new CloseIQProvider());
        pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataPacketProvider());
        */



        pm.addIQProvider("open","http://jabber.org/protocol/ibb", new OpenIQProvider());
        //
         pm.addIQProvider("close","http://jabber.org/protocol/ibb", new CloseIQProvider());
         pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataFormProvider());

        //pm.addExtensionProvider("data", "http://jabber.org/protocol/ibb", new DataPacketProvider());

        //
        // Privacy
        pm.addIQProvider("query", "jabber:iq:privacy", new PrivacyProvider());
        pm.addIQProvider("command", "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider());
        pm.addExtensionProvider("malformed-action",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.MalformedActionError());
        pm.addExtensionProvider("bad-locale",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadLocaleError());
        pm.addExtensionProvider("bad-payload",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadPayloadError());
        pm.addExtensionProvider("bad-sessionid",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.BadSessionIDError());
        pm.addExtensionProvider("session-expired",
                "http://jabber.org/protocol/commands",
                new AdHocCommandDataProvider.SessionExpiredError());

        /*
        DeliveryReceiptManager dm=DeliveryReceiptManager.getInstanceFor(mConnection);
        dm.enableAutoReceipts();
        dm.registerReceiptReceivedListener(new ReceiptReceivedListener() {
                                               public void onReceiptReceived(String fromJid, String toJid, String receiptId) {
                                                   Log.d(SmackImpl.class, "got delivery receipt for " + receiptId);
                                                   changeMessageDeliveryStatus(receiptId, ChatConstants.DS_ACKED);
                                               }
                                           }
        );
        */

        //SmackConfiguration.setKeepAliveInterval(-1);


    }
}

