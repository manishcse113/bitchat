package com.bitskey.grossbit.chat.gen;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.bitskey.grossbit.chat.R;
import com.bitskey.grossbit.chat.chat.ChatBubbleActivity;
import com.bitskey.grossbit.chat.db.ChatRecord;

import java.util.Map;

//import com.bitskey.grossbit.chat.chat2.MainActivity;

// Shows all the users activity.
public class ChatUsersListActivity extends AppCompatActivity implements UserListLayout.UserListCallback {
//extends AppCompatActivity {
    private BroadcastReceiver bReceiver;
    private LocalBroadcastManager bManager;

    //MyReceiver myReceiver;
    private final String FILENAME = "GBC UsersListActivity#";
    UserMaps mUsersMap;
    UserListLayout simpleAdpt = null;
    String[] userNameList;
    Integer[] imageList;
    public static final String ACTION_CHAT_SEND = "com.bitskey.grossbit.chat.action.CHATSEND";
    public static final String ACTION_CHAT_RECEIVE = "com.bitskey.grossbit.chat.action.CHATRECEIVE";
    public static final String ACTION_CHAT_START = "com.bitskey.grossbit.chat.action.CHATSTART";
    public static final String ACTION_CHAT_START_EXTERNAL_FILE_SHARE = "com.bitskey.grossbit.chat.action.CHATSTARTEXTERNALFILESHARE";
    public static final String ACTION_CHAT_START_INTERNAL_FILE_SHARE = "com.bitskey.grossbit.chat.action.CHATSTARTINTERNALFILESHARE";
    public static final String ACTION_CHAT_START_EXTERNAL_TEXT_SHARE = "com.bitskey.grossbit.chat.action.CHATSTARTEXTERNALTEXTSHARE";
    public static final String ACTION_CHAT_START_INTERNAL_TEXT_SHARE = "com.bitskey.grossbit.chat.action.CHATSTARTINTERNALTEXTSHARE";
    public static final String REMOTE_USER_NAME = "com.bitskey.grossbit.chat.extra.REMOTE_USER_NAME";
    public static final String MY_NAME = "com.bitskey.grossbit.chat.extra.MY_NAME";
    public static final String EXTERNAL_FILE = "com.bitskey.grossbit.chat.extra.IS_EXT_FILE";
    public static final String EXTERNAL_TEXT = "com.bitskey.grossbit.chat.extra.IS_EXT_TEXT";
    public static final String MIME_TYPE = "com.bitskey.grossbit.chat.extra.MIME_TYPE";
    private boolean mBoolLoadDB;
    private ActionBar actionBar;
    public static boolean mIsUserSelectedForSharing = false;
    public static String mSelectedUserName = null;
    public static int mSelectedPosition = 0;
    public void showActionBar()
    {
        actionBar.show();
    }
    public void hideActionBar()
    {
        actionBar.hide();
    }
    @Override
    public void userSelected(int position, String userName) {
        mIsUserSelectedForSharing = true;// This is set to true - while selecting sharing buttong
        mSelectedUserName = userName;
        mSelectedPosition = position;
    }
    @Override
    public void userDeSelected(int position, String userName) {
        mIsUserSelectedForSharing = false;// This is set to true - while selecting sharing buttong
        mSelectedUserName = userName;
        mSelectedPosition = position;
        //hideActionBar();
    }
    @Override
    public void activateDefaultMenu() {
        //hideActionBar();
        setDefauleMenu(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.invalidateOptionsMenu();
        }
    }
    @Override
    public void activateSelectedMenu() {
        //hideActionBar();
        setDefauleMenu(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.invalidateOptionsMenu();
        }
    }
    /*
    private class MyReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context arg0, Intent arg1) {
            //DO Auto-generated method stub

            int datapassed = arg1.getIntExtra("DATAPASSED", 0);
            handleIntent(arg1);
        }
    }
    */

    /*
    @Override
    public void onResume()
    {
        IntentFilter filterSend = new IntentFilter();
        filterSend.addAction(ChatXMPPService.ACTION_USERLIST);
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        bManager.registerReceiver(bReceiver, filterSend);
    }

    @Override
    public void onPause()
    {
       // IntentFilter filterSend = new IntentFilter();
        //filterSend.addAction(ChatXMPPService.ACTION_USERLIST);
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        bManager.unregisterReceiver(bReceiver);
    }
    */

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        /*
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatXMPPService.ACTION_USERLIST);
        registerReceiver(myReceiver, intentFilter);
        */
        super.onStart();
        //For communication between service ane activity
        //Following string is also defined in manifest.xml

        /**
         * Messenger used for receiving responses from service.
         */
        // explicit Intent, safe
        /*
        Intent serviceIntent = new Intent(ChatXMPPService.class.getName());
        serviceIntent.setPackage("com.bitskey.grossbit.chat");
        boolean bindResult = getApplicationContext().bindService(serviceIntent, mConn, Context.BIND_AUTO_CREATE);
        */
        // bindService(new Intent("com.bitskey.grossbit.chat.StartService"), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "onStart");
    }
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        //unregisterReceiver(myReceiver);
        super.onStop();
        Log.d(FILENAME, "onStop");
        //following commented: as it seems crashing here.
        /*
        if (mServiceConnected) {
            //unbindService(mConn);
            doUnbindService();
            // Manish: Service need not be stopped - as it is being used by other activites.
            //stopService(new Intent(this, ChatXMPPService.class));
            mServiceConnected = false;
        }
        */
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatusers_list);
        Log.d(FILENAME, "onCreate");

/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.my_toolbar);
       if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle(getTitle());
            toolbar.setNavigationIcon(R.drawable.ic_launcher);
            getSupportActionBar().setLogo(R.drawable.ic_launcher);
            getSupportActionBar().setDisplayUseLogoEnabled(true);

            //actionBar.hide();
            mDefaultMenu = true;
            showActionBar();
            activateDefaultMenu();

        }
*/
        /*
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        actionBar = getSupportActionBar();
        actionBar.hide();
        */

        /*
        bManager = LocalBroadcastManager.getInstance(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatXMPPService.ACTION_USERLIST);
        */

        //To receive intents for presence. it will receive intents only if activity is active.
        //unregister it while destroying the activity.

        /*
        bReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Toast.makeText(context, "Presence Intent!!", Toast.LENGTH_SHORT).show();
                handleIntent(getIntent());
            }
        };
        IntentFilter filterSend = new IntentFilter();
        filterSend.addAction(ChatXMPPService.ACTION_USERLIST);
        bManager = LocalBroadcastManager.getInstance(getBaseContext());
        bManager.registerReceiver(bReceiver, filterSend);
        */
        //registerReceiver(bReceiver, filterSend);
        // We get the ListView component from the layout

        doBindService();
        handleIntent(getIntent());

        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);

        /*
        myList = (ArrayList<String>) getIntent().getSerializableExtra("mylist");
        initList();
        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);
        // This is a simple adapter that accepts as parameter
        // Contsext
        // Data list
        // The row layout that is used during the row creation
        // The keys used to retrieve the data
        // The View id used to show the data. The key number and the view id must match
        simpleAdpt = new SimpleAdapter(this, usersList, android.R.layout.simple_list_item_1, new String[]{"User"}, new int[]{android.R.id.text1});
        lv.setAdapter(simpleAdpt);
        */
        // we register for the contextmneu
        registerForContextMenu(lv);
    }

    public void handleIntent(Intent intent) {
        Log.d(FILENAME, "handleIntent");

        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) { //If external sharing events received:
            //Process - only if logged in
            if (simpleAdpt != null) {
                mMimeType = type;
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
                else if (type.startsWith("application/pdf")) {
                    handleSendImage(intent); // Handle single image being sent
                }
                else if (type.startsWith("video/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
            }
            else { //login first
                Toast.makeText(this, "Login required" , Toast.LENGTH_SHORT).show();
            }

        }
        else if (ChatXMPPService.ACTION_USERLIST.equals(action))// intent received from login activity
        {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                processBundleMsg(extras);
            }
        }
        else
        {
            //It comes here - on clicking notification event: in which case it is probably not bind to service:
            //doBindService();
            sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST);
        }
    }

    static public boolean mIsFileSelectedForSharing = false;
    static public String mfileNameWithPath = null;
    Uri imageUri = null;
    static public boolean mIsTextSelectedForSharing = false;
    static public String mTextToSend = null;
    static public String mMimeType = null;
    //Process intents from externs apps for sharing files

    //Process intents from externs apps for sharing files
    void handleSendText(Intent intent) {
        mTextToSend = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (mTextToSend != null) {
            mIsTextSelectedForSharing = true;
            // Update UI to reflect text being shared
            //Save this information to pass to the user being selected...
        }
    }
    void handleSendImage(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            mIsFileSelectedForSharing = true;
            mfileNameWithPath = imageUri.getPath();
            // Update UI to reflect image being shared
            //Save this information to pass to the user being selected...
        }
    }

    public void processBundleMsg(Bundle extras) {
        Log.d(FILENAME, "processBundleMsg");
        if (extras != null) {
            mUsersMap = (UserMaps) extras.getSerializable("userPresenceMap");
            // do something with the customer
            if (mUsersMap == null)
            {
                Log.d(FILENAME, "mUsersMap is empty");
                return;
            }
            Map<String, String> map = mUsersMap.mUserPresenceMap;
            userNameList = new String[map.size()];
            imageList = new Integer[map.size()];
            Log.d(FILENAME, "handleIntent, userMapSize = " + map.size());
            int i = 0;
            for (Map.Entry<String, String> entry : map.entrySet()) {
                userNameList[i] = entry.getKey();
                Log.d(FILENAME, "handleIntent, user name " + userNameList[i] + " presence" + entry.getValue());
                if (entry.getValue().equals("offline")) {
                    Log.d(FILENAME, "handleIntent, presence " + "offline");
                    imageList[i] = R.drawable.offline;
                } else if (entry.getValue().equals("online")) {
                    Log.d(FILENAME, "handleIntent, presence " + "online");
                    imageList[i] = R.drawable.available;
                } else {
                    Log.d(FILENAME, "handleIntent, presence " + "away");
                    imageList[i] = R.drawable.away;
                }
                i++;
            }
        }
        // We get the ListView component from the layout
        ListView lv = (ListView) findViewById(R.id.listView);
        // This is a simple adapter that accepts as parameter
        // Contsext
        // Data list
        // The row layout that is used during the row creation
        // The keys used to retrieve the data
        // The View id used to show the data. The key number and the view id must match
        //simpleAdpt = new SimpleAdapter(this, mUsersList, android.R.layout.simple_list_item_1, new String[]{"User"}, new int[]{android.R.id.text1});
        //simpleAdpt = new UserListLayout(this, R.layout.userlistlayout, R.id.username, userStrList);
        //String[] dummy = {"a"};
        //simpleAdpt = new UserListLayout(this, R.layout.userlistlayout, R.id.username,dummy, mUsersListMap);
        simpleAdpt = new UserListLayout(this, R.layout.userlistlayout, R.id.username, userNameList, imageList, mUsersMap);
        lv.setAdapter(simpleAdpt);

        simpleAdpt.setCallback(this);

        /*
        lv.setOnTouchListener(new View.OnTouchListener(){
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                sendToService(ChatXMPPService.MESSAGE_TYPE_USERLIST_QUERY);
                                return false;
                            }
                        });
        */
        // we register for the contextmneu
        registerForContextMenu(lv);
    }

    @Override
    protected void onNewIntent(Intent intent) {

        Log.d(FILENAME, "onNewIntent");
        handleIntent(intent);
    }

    @Override
    protected void onDestroy() {
        Log.d(FILENAME, "onDestroy");
        //unregisterReceiver(bReceiver);
        sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
       // bManager.unregisterReceiver(bReceiver);
        super.onDestroy();
    }


    /*
    // React to user clicks on item
    public void setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView p, View view, int position,long id)
            {

            // We know the View is a TextView so we can cast it
            //TextView clickedView = (TextView) view;
            //Toast.makeText(MainActivity.this, "Item with id ["+id+"] - Position ["+position+"] - Planet ["+clickedView.getText()+"]", Toast.LENGTH_SHORT).show();
            }
        });
        */


    // We want to create a context Menu when the user long click on an item
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Log.d(FILENAME, "onCreateContextMenu");
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        // We know that each row in the adapter is a Map
        //HashMap map = (HashMap)
        String selectedUserName =     simpleAdpt.getItem(aInfo.position);
        menu.setHeaderTitle("Information for " + selectedUserName);

        Log.d(FILENAME, "onCreateContextMenu " + selectedUserName);
        menu.add(aInfo.position, 1, 1, selectedUserName);
       // menu.add(aInfo.position, 2, 2, "Block/Unblock");
        menu.add(aInfo.position, 3, 3, "Chat");
        //menu.add(aInfo.position, 4, 4, "Add to Contacts");
        menu.add(aInfo.position, 5, 5, "Clear history All");
        //menu.add(aInfo.position, 6, 6, "Clear history before 3 months");
        //menu.add(aInfo.position, 7, 7, "Clear history before 1 month");
        //menu.add(aInfo.position, 8, 8, "Clear history before 1 week");
        //menu.add(aInfo.position, 9, 9, "Clear history last week");
        //menu.add(aInfo.position, 10, 10, "Clear history today");
        //menu.add(aInfo.position, 11, 11, "Back");
    }

    // This method is called when user selects an Item in the Context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(FILENAME, "onContextItemSelected");
        int itemId = item.getItemId();
        int selectedposition = item.getGroupId();
        String selectedUserName =     simpleAdpt.getItem(selectedposition);

        switch(itemId)
        {
            case 1: //user Detailed information
                Toast.makeText(this, "Item id [" + itemId + "]" + selectedUserName , Toast.LENGTH_SHORT).show();
                break;
            case 2: // Block/Unblock User
                break;
            case 3: // Chat with user
                UserListLayout.startActionChat(this, ChatXMPPService.mUserName.toString(), selectedUserName);
                break;
            case 4: // Add to contact list
                break;
            case 5: //Clear history All
                //item.setTitleCondensed()
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString(), selectedUserName, false);
                break;
            case 6: //Clear history before 3 month
                break;
            case 7: //Clear history before 1 month
                break;
            case 8: //Clear history before 1 week
                 break;
            case 9: //Clear history of last 1 week
                break;
            case 10: //Clear history today
                break;
            case 11: //back
                break;
            default:
                return super.onContextItemSelected(item);
        }

        //item.setTitleCondensed(ChatXMPPService.mUserName);
        //Log.d(FILENAME, "StartingChat param1" + ChatXMPPService.mUserName + "param2 " + item.getTitle().toString());
        // Implements our logic

        //startActionChat(this.getBaseContext(), item.getTitleCondensed().toString(), item.getTitle().toString());
        return true;
    }


    // TODO: Customize helper method\\\\\\\\\
    void startActionChat(Context context, String param1, String param2) {
        Log.d(FILENAME, "startActionChat");
        //void startActionChat(String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        Intent intent = new Intent(getApplicationContext(), ChatBubbleActivity.class);
        intent.setAction(ACTION_CHAT_START);
        intent.putExtra(MY_NAME, param1);
        intent.putExtra(REMOTE_USER_NAME, param2);
        startActivity(intent);
    }


    /**
     * Messenger used for communicating with service.
     */
    Messenger mService = null;
    boolean mServiceConnected = false;

    //public boolean mIsFileSharingSelected = false;
    //public String mfileSharingName = null;
    /**
     * Class for interacting with the main interface of the service.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST) {
                Log.d(FILENAME,"IncomingHandler : MESSAGE_TYPE_USERLIST " );
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            /*
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST)
            { //Share message from other user
                Log.d(FILENAME,"IncomingHandler : MESSAGE_TYPE_SHARE_FILE" );
                Bundle b = msg.getData();
                //Log.d(FILENAME, "processBundleMsg");
                if (b != null) {
                    mfileSharingName = (String) b.getSerializable("filenamewithPath");
                    mIsFileSharingSelected = true;
                }

            }
            */
            else {
                super.handleMessage(msg);
            }
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mServiceConnected = true;
            // Register our messenger also on Service side:
            Message msg = Message.obtain(null,
                    ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST);
            msg.replyTo = mMessenger;
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        }

        /**
         * Connection dropped.
         */
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(FILENAME, "Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }


    };
    /**
     * Sends message with text stored in bundle extra data ("data" key).
     *
     * @param  code to send
     */
    void sendToService(int code) {
        if (mServiceConnected) {

            Message msg = null;
            if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_UNREGISTER");
                //Manish: Send from here, whatever to send to service from activity:
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            } else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_USERLIST_QUERY");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        } else {
            Log.d(FILENAME, "Cannot send - not connected to service.");
        }
    }


    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.d(FILENAME, "binding initiated");
        mIsBound = getApplicationContext().bindService(new Intent(getApplicationContext(), ChatXMPPService.class), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "binding done");
        //mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            Log.d(FILENAME, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
            }
            // Detach our existing connection.
            unbindService(mConn);
            mIsBound = false;
        }
    }


    private Menu mMenu;
    private MenuItem mMIBackButton;
    private MenuItem mChat;
    private MenuItem mClearUserHistory;
    private MenuItem mClearAllUsersHistory;
    private MenuItem mMISettings;

    public static boolean mDefaultMenu = true;
    public static void setDefauleMenu(boolean flag)
    {
        mDefaultMenu = flag;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_users, menu);
        mMenu = menu;
        //mDefaultMenu = true;
        mMIBackButton = menu.findItem(R.id.action_backbutton);
        //mChat = menu.findItem(R.id.action_chat);
        mClearUserHistory = menu.findItem(R.id.action_clean_history);
        mClearAllUsersHistory = menu.findItem(R.id.action_clean_all_users_history);
        mMISettings = menu.findItem(R.id.action_settings);
        //activateDefaultMenu();
        //menu.add()
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mDefaultMenu)
        {
            mMIBackButton.setVisible(true);
            //mChat.setVisible(false);
            mClearUserHistory.setVisible(false);
            mClearAllUsersHistory.setVisible(true);
            mMISettings.setVisible(true);
        }
        else
        {
            mMIBackButton.setVisible(true);
            //mChat.setVisible(true);
            mClearUserHistory.setVisible(true);
            mClearAllUsersHistory.setVisible(false);
            mMISettings.setVisible(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       // mIsFileSelectedForSharing = false;
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_backbutton:
                // User chose the "Settings" item, show the app settings UI...
                onBackPressed();
                //hideActionBar();
                activateDefaultMenu();
                return true;
            case R.id.action_clean_all_users_history:
                //hideActionBar();
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString());
                activateDefaultMenu();
                return true;
            case R.id.home:
                onBackPressed();
                activateDefaultMenu();
                return true;
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                activateDefaultMenu();
                return true;
            default://Back button - implemented:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                activateDefaultMenu();
                //hideActionBar();
                return super.onOptionsItemSelected(item);
        }
    }
}