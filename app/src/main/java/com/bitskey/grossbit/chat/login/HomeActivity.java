package com.bitskey.grossbit.chat.login;

/**
 * Created by GUR11219 on 02-07-2015.
 */

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.bitskey.grossbit.chat.R;

/**
 * Home Screen Activity
 */
public class HomeActivity extends Activity {

    private final String FILENAME = "GBC: homeActivity#";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(FILENAME, "onCreate");
        //Displays Home Screen
        setContentView(R.layout.home);
    }

}
