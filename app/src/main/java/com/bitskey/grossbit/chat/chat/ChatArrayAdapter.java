package com.bitskey.grossbit.chat.chat;

/**
 * Created by GUR11219 on 28-09-2015.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ImageSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bitskey.grossbit.chat.R;

import java.util.ArrayList;
import java.util.List;

public class ChatArrayAdapter extends ArrayAdapter {

    private TextView chatText;
    private List chatMessageList = new ArrayList();
    private LinearLayout singleMessageContainer;

    private ChatListCallback callback;

    private final String FILENAME = "GBC ChatArrayAdapter#";

    //@Override
    public void add(ChatMessage object) {
        chatMessageList.add(object);
        super.add(object);
    }

    //@Override
    public void update(ChatMessage object) {
        //search message based on unique id: receiptId and replace the contents.
        for (Object chatMessage : chatMessageList) {
            if (((ChatMessage)(chatMessage)).receiptId == ((ChatMessage)(object)).receiptId)
            {
                //cop from object to the existing message: Ideally only receiptstatus shall change.
                ((ChatMessage)(chatMessage)).receiptStatus = ((ChatMessage)(object)).receiptStatus;
                break;
            }
        }
        //chatMessageList.add(object);
        //super.notify();
    }

    //@Override
    public void delete(ChatMessage object) {
        int index = chatMessageList.indexOf(object);
        chatMessageList.remove(index);
        super.remove(object);
    }

    //@Override
    public void delete(int position) {
        int index = position;
        chatMessageList.remove(index);
        super.remove(index);
    }

    //@Override
    public void update(String receiptId, String receiptStatus) {
        if (receiptId == null)
        {
            return;
        }
        for (int i = 0; i < chatMessageList.size(); i++) {
            ChatMessage cMessage = (ChatMessage)chatMessageList.get(i);
            if (cMessage.receiptId == null )
            {
                continue;
            }
            if (cMessage.receiptId.equals(receiptId))
            {
                cMessage.receiptStatus = receiptStatus;
                break;
            }
        }
        //int index = chatMessageList.get(object);
        //chatMessageList.remove(index);
        //super.remove(object);
    }

    public ChatArrayAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }

    //Not implemented for now - auto send of not delivered messages::
    private void monitoringChatMessagesThread()
    {
        new Thread() {
            @Override
            public void run() {
                while(true) {
                    for (int i = 0; i < chatMessageList.size(); i++) {
                        ChatMessage cMessage = (ChatMessage) chatMessageList.get(i);
                        if (cMessage.receiptStatus.equals(ChatBubbleActivity.MESSAGE_RESEND_REQUIRED)) {
                            // initiate a fresh resend request
                            // in case of file - it will be file resend request::
                            // in case of message - it will be a send request to service::
                        }
                    }
                    try {
                        Thread.sleep(60000L);
                    }
                    catch (Exception e)
                    {
                        Log.e(FILENAME, e.getMessage());
                    }
                }
            }
        }.start();
    }

    public int getCount() {
        return this.chatMessageList.size();
    }

    public ChatMessage getItem(int index) {
        ChatMessage cMessage = (ChatMessage) chatMessageList.get(index);
        return cMessage;
    }

    public View getView(final int position, View convertView, final ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.chatlistlayout2, parent, false);
        }
        singleMessageContainer = (LinearLayout) row.findViewById(R.id.singleMessageContainer);
        final ChatMessage chatMessageObj = (getItem(position));
        chatText = (TextView) row.findViewById(R.id.singleMessage);

        if (chatMessageObj.mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
            chatText.setText(chatMessageObj.message);
            chatText.append("\n" + chatMessageObj.timeStamp);
        } else {
            //Bitmap bm;// = getBitmap("file", chatMessageObj.)
            //d.setAlpha(1);
            //try {
            chatText.setText(chatMessageObj.fileName + "\n");
             if (chatMessageObj.filePath!= null &&
                     (       chatMessageObj.mimeType.equals("image/bmp") ||
                             chatMessageObj.mimeType.equals("image/gif") ||
                             chatMessageObj.mimeType.equals("image/png") ||
                             chatMessageObj.mimeType.equals("image/jpg") ||
                             chatMessageObj.mimeType.equals("image/jpeg") ||
                             chatMessageObj.mimeType.equals("image/x-ms-bmp") ||
                             chatMessageObj.mimeType.equals("image/x-ms-gif") ||
                             chatMessageObj.mimeType.equals("image/x-ms-jpg") ||
                             chatMessageObj.mimeType.equals("image/x-ms-jpeg") ||
                                     chatMessageObj.mimeType.equals("image/x-ms-png")
                     )
                     ){
                 appendDrawable(chatText, chatMessageObj.filePath);
             }
            else
             {
                 appendDrawableIcon(chatText );
                 // rely on other providers for mime-type
                 // display and send intent to other users on click
             }


            chatText.append("\n SizeDisk:" + chatMessageObj.strFileSizeDisk);
            chatText.append( "\n" +  chatMessageObj.timeStamp);
                //chatText.setCompoundDrawablesWithIntrinsicBounds(0, 0, 1, 0);// wherever u want the image relative to textview
            //} catch (IOException e) {
            //    Log.e(FILENAME, "Remote Image Exception", e);
           // }
        }
        if (chatMessageObj.direction == ChatBubbleActivity.MESSAGEDIRECTIONSENT)
        {
            if (chatMessageObj.receiptStatus.equals(ChatBubbleActivity.MESSAGE_ACKED))
            {
                chatText.setBackgroundResource(R.drawable.bubbleadelivered);
            }
            else if (chatMessageObj.receiptStatus.equals(ChatBubbleActivity.MESSAGE_ACK_AWAITED))
            {
                chatText.setBackgroundResource(R.drawable.bubbleaawaitingack);
            }
            else if (chatMessageObj.receiptStatus.equals(ChatBubbleActivity.MESSAGE_RESEND_REQUIRED))
            {
                chatText.setBackgroundResource(R.drawable.bubblearetrying);
            }
            else if (chatMessageObj.receiptStatus.equals(ChatBubbleActivity.MESSAGE_FAILED_PERMANENT))
            {
                chatText.setBackgroundResource(R.drawable.bubbleafailed);
            }
        }
        else
        {
            chatText.setBackgroundResource(R.drawable.bubbleb);
        }
        //chatText.setBackgroundResource(chatMessageObj.direction == ChatBubbleActivity.MESSAGEDIRECTIONSENT ? R.drawable.bubblea : R.drawable.bubbleb);
        singleMessageContainer.setGravity(chatMessageObj.direction == ChatBubbleActivity.MESSAGEDIRECTIONRECEIVED ? Gravity.LEFT : Gravity.RIGHT);
        chatText.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View arg0) {

                callback.activateSelectedMenu();
                if (!chatMessageObj.mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT) //any other file
                        ) {
                    callback.imageSelected(position, chatMessageObj.filePath, chatMessageObj.mimeType, chatMessageObj.receiptId);
                }
                else //Text
                {
                    callback.textSelected(position, chatMessageObj.message, chatMessageObj.mimeType, chatMessageObj.receiptId);
                }
                return true;
            }
        });
        chatText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                callback.activateDefaultMenu();
                if (!chatMessageObj.mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT) //any other file
                ) {
                    callback.imageDeSelected(position, chatMessageObj.filePath, chatMessageObj.mimeType, chatMessageObj.receiptId);
                    ChatBubbleActivity.setDefauleMenu(true);
                }
                else //text
                {
                    callback.textDeSelected(position, chatMessageObj.message, chatMessageObj.mimeType, chatMessageObj.receiptId);
                }
            }
        });
        return row;
    }

    private void appendDrawable(TextView tView, String filePath) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String THREE_SPACES = "   \n";
        builder.append(THREE_SPACES);
        Drawable drawable;

        try {
            drawable = Drawable.createFromPath(filePath); // It crashed here.. as was unable to allocate memory
        }
        catch(Exception e)
        {
            Log.e(FILENAME, e.getMessage());
            return;
        }
                //getContext().getResources().getDrawable(drawableId);
        if (drawable == null)
        {
            return;
        }

        float width = drawable.getIntrinsicWidth();
        float height = drawable.getIntrinsicHeight();
        float scaledownratio = 1;
        if ((width > height) && (width > 400)) {
             scaledownratio = width / 400;
        }
        else if (height> 400)
        {
            scaledownratio = height/400;
        }
        width = width/scaledownratio;
        height = height/scaledownratio;

        //int height = drawable.getIntrinsicHeight()> 360?360: drawable.getIntrinsicHeight();
        drawable.setBounds(0, 0, (int) width,(int) height);
        ImageSpan image = new ImageSpan(drawable);
        builder.setSpan(image, 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tView.append(builder);
    }
    private void appendDrawableIcon(TextView tView) {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        String THREE_SPACES = "   \n";
        builder.append(THREE_SPACES);
        //Drawable drawable = Drawable.createFromPath(filePath);
        //getContext().getResources().getDrawable(drawableId);
        //drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        ImageSpan image = new ImageSpan(getContext(),R.drawable.fileicon);
        builder.setSpan(image, 0, 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tView.append(builder);
    }


    /*
    private File getBitmap(String tag, Uri url)
    {
        File cacheDir;
        // if the device has an SD card
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
        } else {
            // it does not have an SD card
            //cacheDir=ActivityPicture.this.getCacheDir();
            cacheDir=context.getCacheDir();
        }
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        //Get FileName
        String fileName = null;
        if (url.getScheme().equals("file")) {
            fileName = url.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(url, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                    Log.d(FILENAME, "name is " + fileName);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        File f;
        try {
            Bitmap bitmap=null;
            InputStream is = null;
            if (url.toString().startsWith("content://com.google.android.gallery3d") ||
                    url.toString().startsWith("content://com.android.providers")
                    ) {
                is=context.getContentResolver().openInputStream(url);
            } else {
                is=new URL(url.toString()).openStream();
            }
            if (fileName!=null) {
                f = new File(cacheDir, fileName);
            }
            else
                f = new File(cacheDir, tag);
            OutputStream os = new FileOutputStream(f);
            //Utils.CopyStream(is, os);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            //f.
            //return null;
            //is.decodeStream
            return f;
        } catch (Exception ex) {
            Log.d(FILENAME, "Exception: " + ex.getMessage());
            // something went wrong
            ex.printStackTrace();
            return null;
        }
    }
    */

    public Bitmap decodeToBitmap(byte[] decodedByte) {
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public void setCallback(ChatListCallback callback){
        this.callback = callback;
    }
    public interface ChatListCallback {
        public void imageSelected(int position, String fileNameWithPath, String mimeType, String receiptId);
        public void imageDeSelected(int position, String fileNameWithPath, String mimeType, String receiptId);
        public void textSelected(int position, String text, String mimeType, String receiptId);
        public void textDeSelected(int position, String text, String mimeType, String receiptId);
        public void activateDefaultMenu();
        public void activateSelectedMenu();
    }
}
