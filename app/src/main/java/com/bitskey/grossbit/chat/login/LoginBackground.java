package com.bitskey.grossbit.chat.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.bitskey.grossbit.chat.db.MyRecord;
import com.bitskey.grossbit.chat.gen.ChatXMPPService;

/**
 * Created by GUR11219 on 22-09-2015.
 */

class LoginBackground extends AsyncTask<Void,Void,Void> {
    private ProgressDialog progress;
    private final String FILENAME = "GBC LoginBackground#";
    private String userName;
    private String password;
    private Context context;
    //static boolean autologin = false;

    LoginBackground(LoginActivity activity)
    {
        Log.d(FILENAME, "LoginBackground");
        progress = new ProgressDialog(activity);
        userName = activity.mUserName;
        password = activity.mPassword;
        context = activity.getApplicationContext();
    }

    @Override
    protected Void doInBackground(Void...arg){
        Log.d(FILENAME, "doInBackground");
        publishProgress();
        // do your processing here like sending data or downloading etc.
        ChatConnection connection = ChatConnection.getInstance();
        connection.connect(context, userName, password);
        boolean done = false;
        while(!done)
        {
            if(!(connection.getStatus() == ChatConnection.Status.RUNNING))
            {
                done=true;
            }
            /*
            else
            {
                Log.v(FILENAME, "Network not Available!");
            }*/
        }
        Log.d(FILENAME, "doInBackground exit");
        return null;
    }
    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        Log.d(FILENAME, "onPreExecute");
        //progress = new ProgressDialog();
        progress.setMessage("Loging in ...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();// It needs to be fixed. It crashed here on progress.dismiss();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        Log.d(FILENAME, "onProgressUpdate");
        progress.show();
    }
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d(FILENAME, "onPostExecute starting login");
        if (progress!=null && progress.isShowing()) {
            progress.dismiss();
        }
        progress = null;
        ChatConnection connection = ChatConnection.getInstance();
        if(connection.getStatus() == ChatConnection.Status.FINISHED) {

            //Remove old records, persists new records
            MyRecord.clearFromDB(context);
            MyRecord.storeRecord(context, userName, null, password, null, null);//group, mail, avatar are null for now
			 Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show();
            Log.d(FILENAME, "username and password are " + userName + " " + password);
            //autologin = true;
            ChatXMPPService.startActionLogin(context, userName, password, ChatXMPPService.STR_LOGGEDIN);
        }
        else
        {
            Toast.makeText(context, "Login Failed: Taking long time - Check Server/Network or login credentials - continuing in offline mode", Toast.LENGTH_SHORT).show();
            //Show tost to retry login: again: warning message:
            if (LoginActivity.autologin == true) {
                ChatXMPPService.startActionLogin(context, userName, password, "anything");
            }
        }
        //ChatXMPPService.startAction
    }

    //@Override
    public void onPause() {
        //super.onPause();
        if ((progress != null) && progress.isShowing())
            progress.dismiss();
        progress = null;
    }
}