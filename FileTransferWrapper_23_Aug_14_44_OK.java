package com.bitskey.grossbit.chat;

import android.os.Environment;
import android.util.Log;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.filetransfer.FileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import java.io.File;

/**
 * Created by GUR11219 on 07-08-2015.
 */
public class FileTransferWrapper {

    private static String FILENAME = "GBC FileTransferWrapper#";

    //Working code

    public static void SendFile(String filenameWithPath,  String userAddress) {
        FileTransferManager manager = FileTransferManager.getInstanceFor(ChatXMPPService.mConnection);

        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer("usre2@myHost/Smack");
        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userAddress + "/Smack");
        //String fullJID = PresenceManager.getFullyQualifiedJID(jid);

        OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userAddress + "/Spark 2.7.0");

        File f = new File(filenameWithPath);
        if (!f.exists()) {
            Log.d(FILENAME, "File does not exists" + " filepath# " + filenameWithPath);
        }
        if (!f.canRead()) {
            Log.d(FILENAME, "File can not be read" + " filepath# " + filenameWithPath);
        }
        try {
            transfer.sendFile(f, "MyImage.png");
        } catch (SmackException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Log.d(FILENAME, "Sending file exceptption" + " filepath# " + filenameWithPath + " filename# " + "Test" + e.getMessage());
        }
        while (!transfer.isDone()) {
            if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)) {
                System.out.println("ERROR!!! " + transfer.getError());
            } else if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.cancelled)
                    || transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.refused)) {
                System.out.println("Cancelled!!! " + transfer.getError());
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.refused) || transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)
                || transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.cancelled)) {
            System.out.println("refused cancelled error " + transfer.getError());
        } else {
            System.out.println("Success");
        }
    }


    /*
    // Fails at Log.e("ERROR!!! ", transfer.getError() + "");
    public static void ReceiveFile()
    {
        FileTransferManager manager = FileTransferManager.getInstanceFor(ChatXMPPService.mConnection);
        manager.addFileTransferListener(new FileTransferListener()
        {
            public void fileTransferRequest(final FileTransferRequest request)
            {
               new Thread()
               {
                    @Override
                    public void run()
                    {
                        IncomingFileTransfer transfer = request.accept();
                        File mf = Environment.getExternalStorageDirectory();
                        File file = new File(mf.getAbsoluteFile()+"/DCIM/Camera/" + transfer.getFileName());
                        try
                        {
                             transfer.recieveFile(file);
                             while(!transfer.isDone())
                             {
                                try
                                {
                                   Thread.sleep(1000L);
                                }
                                catch (Exception e)
                                {
                                   Log.e("", e.getMessage());
                                }
                                if(transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error))
                                {
                                   Log.e("ERROR!!! ", transfer.getError() + "");
                                }
                                if(transfer.getException() != null)
                                {
                                   transfer.getException().printStackTrace();
                                }
                             }
                        }catch (Exception e)
                        {
                             Log.e("", e.getMessage());
                        }
                    };
                }.start();
                 }
            }
        );
    }
*/

    /*
    public static void ReceiveFile( ) {
        XMPPConnection connection2 = ChatXMPPService.mConnection;
        // TODO Auto-generated method stub
        if(connection2 != null) {
            ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection2);
            if (sdm == null) {
                //sdm = new ServiceDiscoveryManager(connection2);
            }
            sdm.addFeature("http://jabber.org/protocol/disco#info");
            sdm.addFeature("jabber:iq:privacy");
            FileTransferManager manager = FileTransferManager.getInstanceFor(connection2);
            FileTransferNegotiator fTN = FileTransferNegotiator.getInstanceFor(connection2);

            manager.addFileTransferListener(new FileTransferListener() {

                public void fileTransferRequest(final FileTransferRequest request) {
                    new Thread() {

                        @Override
                        public void run() {
                            IncomingFileTransfer transfer = request.accept();
                            File mf = Environment.getExternalStorageDirectory();
                            final File file = new File(mf.getAbsoluteFile() + "/" + transfer.getFileName());
                            try {
                                transfer.recieveFile(file);
                                //StreamNegotiator sn = fTN.selectStreamNegotiator();
                                while (!transfer.isDone()) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (Exception e) {
                                        Log.e("", e.getMessage());
                                    }
                                    if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)) {
                                        Log.e("ERROR!!! ", transfer.getError() + "");
                                    }
                                    if (transfer.getException() != null) {
                                        transfer.getException().printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        ;
                    }.start();
                }
            });
        }
    }
*/


/*
//Tested: stuck in negoitation
    public static void ReceiveFile( ) {
        XMPPConnection connection2 = ChatXMPPService.mConnection;
        // TODO Auto-generated method stub
        if(connection2 != null) {
            ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection2);
            if (sdm == null) {
                //sdm = new ServiceDiscoveryManager(connection2);
            }
            sdm.addFeature("http://jabber.org/protocol/disco#info");
            sdm.addFeature("jabber:iq:privacy");
            FileTransferManager manager = FileTransferManager.getInstanceFor(connection2);
            FileTransferNegotiator fTN = FileTransferNegotiator.getInstanceFor(connection2);

            manager.addFileTransferListener(new FileTransferListener() {

                public void fileTransferRequest(final FileTransferRequest request) {
                    new Thread() {

                        @Override
                        public void run() {
                            final IncomingFileTransfer transfer = request.accept();
                            File mf = Environment.getExternalStorageDirectory();
                            final File file = new File(mf.getAbsoluteFile() + "/" + transfer.getFileName());
                            try {
                                transfer.recieveFile(file);
                                //StreamNegotiator sn = fTN.selectStreamNegotiator();
                                new Thread() {
                                    @Override
                                    public void run() {
                                        while (!transfer.isDone()) {
                                            try {
                                                Thread.sleep(1000);
                                            } catch (Exception e) {
                                                Log.e("", e.getMessage());
                                            }
                                            if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)) {
                                                Log.e("ERROR!!! ", transfer.getError() + "");
                                            }
                                            if (transfer.getException() != null) {
                                                transfer.getException().printStackTrace();
                                            }
                                        }
                                    }
                                }.start();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        ;
                    }.start();
                }
            });
        }
    }
*/


/*
//Compilation issue with this function

    public static void ReceiveFile() {
        FileTransferManager fileTransferManager = FileTransferManager.getInstanceFor(ChatXMPPService.mConnection);
        fileTransferManager.addFileTransferListener(new FileTransferListener() {
            @Override
            public void fileTransferRequest(FileTransferRequest request) {
                try {
                    IncomingFileTransfer transfer = request.accept();
                    transfer.recieveFile(new File(getExternalFilesDir(null), transfer.getFileName()));
                    String line;
                    BufferedReader br = new BufferedReader(new FileReader(new File(getExternalFilesDir(null), transfer.getFileName())));
                    while ((line = br.readLine()) != null) {
                        System.out.println(line);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
*/


    /*
    public static void SendFile(final String directory, final String receiver) {
        Thread thread = new Thread() {
            public void run() {
                ServiceDiscoveryManager sdm = ServiceDiscoveryManager
                        .getInstanceFor(ChatXMPPService.mConnection);
                if (sdm == null)
                    sdm = ServiceDiscoveryManager.getInstanceFor(ChatXMPPService.mConnection);
                sdm.addFeature("http://jabber.org/protocol/disco#info");
                sdm.addFeature("jabber:iq:privacy");
                // Create the file transfer manager
                FileTransferManager manager = FileTransferManager.getInstanceFor(
                        ChatXMPPService.mConnection);

                // Create the outgoing file transfer
                OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(receiver
                         + "/Spark 2.7.0");

                //transfer = manager.createOutgoingFileTransfer("amarnath@ec2-54-148-100-240.us-west-2.compute.amazonaws.com" + "/Spark 2.7.0");
                Log.i("transfere file", "outgoingfiletransfere is created");
                try {
                    OutgoingFileTransfer.setResponseTimeout(30000);
                    try {
                        transfer.sendFile(new File(directory), "Description");
                        Log.i("transfere file", "sending file");
                    }
                    catch(SmackException e)
                    {
                        e.printStackTrace();
                    }
                    while (!transfer.isDone()) {
                        try {
                            Thread.sleep(1000);
                            Log.i("transfere file", "sending file status "
                                    + transfer.getStatus() + "progress: "
                                    + transfer.getProgress());
                            if (transfer.getStatus() == FileTransfer.Status.error) {
                                transfer.cancel();
                                break;
                            }
                        } catch (InterruptedException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                };
                Log.i("transfere file", "sending file done");
            }
        };
       thread.start();
    }
    */



    //New function : implemented after mid night: 20-Aug
    public static void ReceiveFile() {
        Thread thread = new Thread() {
            public void run() {
                ServiceDiscoveryManager sdm = ServiceDiscoveryManager
                        .getInstanceFor(ChatXMPPService.mConnection);
                if (sdm == null)
                    sdm = ServiceDiscoveryManager.getInstanceFor(ChatXMPPService.mConnection);
                sdm.addFeature("http://jabber.org/protocol/disco#info");
                sdm.addFeature("jabber:iq:privacy");
                // Create the file transfer manager
                final FileTransferManager managerListner = FileTransferManager.getInstanceFor(
                        ChatXMPPService.mConnection);

                Log.i("File transfere manager", "created");
                // Create the listener
                managerListner
                        .addFileTransferListener(new FileTransferListener() {
                                 public void fileTransferRequest(final FileTransferRequest request) {
                                     Log.i("Recieve File",
                                             "new file transfere request  new file transfere request   new file transfere request");
                                     Log.i("file request",
                                             "from" + request.getRequestor());
                                     IncomingFileTransfer transfer = request.accept();
                                     Log.i("Recieve File dialog", "accepted");
                                     try {

                                         File mf = Environment.getExternalStorageDirectory();
                                         final File file = new File(mf.getAbsoluteFile() + "/" + transfer.getFileName());

                                         try {
                                             //transfer.recieveFile(file);
                                             transfer.recieveFile(new File("/sdcard/" + request.getFileName()));
                                             //transfer.recieveFile(new File("/DCIM/Camera" + request.getFileName()));

                                         } catch (Exception e) {
                                             e.printStackTrace();
                                         }
                                         while (!transfer.isDone() || (transfer.getProgress() < 1)) {
                                             Thread.sleep(1000);

                                             Log.i("Recieve File dialog", "still receiving : "
                                                     + (transfer.getProgress()) + " status "
                                                     + transfer.getStatus());
                                             if (transfer.getStatus().equals(FileTransfer.Status.error)) {
                                                 // Log.i("Error file",
                                                 // transfer.getError().getMessage());
                                                 Log.i("Recieve File dialog",
                                                         "cancelling still receiving : "
                                                                 + (transfer.getProgress())
                                                                 + " status "
                                                                 + transfer.getStatus());
                                                 transfer.cancel();
                                                 break;
                                             }
                                         }
                                     } catch (InterruptedException e) {
                                         // TODO Auto-generated catch block
                                         e.printStackTrace();
                                     } catch (Exception e) {
                                         // TODO Auto-generated catch block
                                         e.printStackTrace();
                                     }
                                 }
                             }
                            );
                        }
            };
            thread.start();
        }


    /*
    //from thread: https://community.igniterealtime.org/message/241154#241154
    public static void ReceiveFile() {
        Thread thread = new Thread() {
            public void run() {
                ServiceDiscoveryManager sdm = ServiceDiscoveryManager
                        .getInstanceFor(ChatXMPPService.mConnection);
                if (sdm == null)
                    sdm = ServiceDiscoveryManager.getInstanceFor(ChatXMPPService.mConnection);
                sdm.addFeature("http://jabber.org/protocol/disco#info");
                sdm.addFeature("jabber:iq:privacy");

                // Create the file transfer manager
                final FileTransferManager managerListner = FileTransferManager.getInstanceFor(
                        ChatXMPPService.mConnection);
                FileTransferNegotiator.getInstanceFor(ChatXMPPService.mConnection);
                Log.i("File transfere manager", "created");
                // Create the listener
                managerListner.addFileTransferListener(new FileTransferListener() {
                    public void fileTransferRequest(final FileTransferRequest request) {
                        Log.i("Recieve File","new file transfere request  new file transfere request   new file transfere request");
                        Log.i("file request","from" + request.getRequestor());
                        IncomingFileTransfer transfer = request.accept();
                        //Log.i("Recieve File alert dialog", "accepted");
                        try {
                            System.out.println("Transfer File Name:" + transfer.getFileName());
                            System.out.println("Request File Name:"+request.getFileName());
                            System.out.println("Resquest desceiption"+ request.getDescription());
                            final String fileName = request.getFileName();
                            try {
                                transfer.recieveFile(new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/QuipIt/" + request.getFileName()));
                            }
                            catch(Exception e)
                            {
                                e.printStackTrace();
                            }
                            Log.e("Saving File ", "Saving fie recived from Xmpp");
                            System.out.println("Transfer Path File Name" + transfer.getFilePath());
                            while (!transfer.isDone() || (transfer.getProgress() < 1)) {
//                      Thread.sleep(1000);
                               // Log.i("Recieve File alert dialog", "still receiving : " + (transfer.getProgress()) + " status " + transfer.getStatus());
                                System.out.println("Trying to receive file");
                                if (transfer.getStatus().equals(FileTransfer.Status.error)) {
                                    // Log.i("Error file",
                                    // transfer.getError().getMessage());
                                    Log.i("Recieve File alert dialog","cancelling still receiving : " + (transfer.getProgress()) + " status " + transfer.getStatus());
                                    System.out.println("While Receiveing file");
                                    System.out.println("Error transfer.getError() : "+transfer.getError());
                                    System.out.println("transfer.getException() : "+transfer.getException());
                                    transfer.cancel();
                                    break;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        };
        thread.start();
    }
        */



}
